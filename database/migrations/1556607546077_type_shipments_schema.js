'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TypeShipmentsSchema extends Schema {
  up () {
    this.create('type_shipments', (table) => {
      table.increments('type_id')
      table.timestamps()

      table.string('name', 100).notNull();
      table.string('type_code', 9).default('TCIND').index('TCIND');
      table.string('icon', 150).nullable();
    })
  }

  down () {
    this.drop('type_shipments')
  }
}

module.exports = TypeShipmentsSchema
