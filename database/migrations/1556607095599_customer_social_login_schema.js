'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSocialLoginSchema extends Schema {
  up () {
    this.create('customer_social_logins', (table) => {
      table.increments()
      table.timestamps()
      table.integer('customer_id');
      table.string('profile_id', 100).default('PIDIND').index('PDIND');
      table.string('network', 32).default('NTWK').index('NTIND');
      table.string('accessToken', 120).default('0000').index('ATIND');
    })
  }

  down () {
    this.drop('customer_social_logins')
  }
}

module.exports = CustomerSocialLoginSchema
