'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CountriesSchema extends Schema {
  up () {
    this.create('countries', (table) => {
      table.increments()
      table.timestamps()
      table.string('country').notNull();
      table.string('country_code', 10).default('CC00').index('CCIND');
    })
  }

  down () {
    this.drop('countries')
  }
}

module.exports = CountriesSchema
