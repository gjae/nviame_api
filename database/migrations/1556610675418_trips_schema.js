'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TripsSchema extends Schema {
  up () {
    this.create('trips', (table) => {
      table.increments()
      table.timestamps()
      table.text('placeid_from').nullable();
      table.text('placeid_to').nullable();
      table.float('lat_to').nullable();
      table.float('lat_from').nullable();
      table.float('long_from').nullable();
      table.float('long_to').nullable();
      table.integer('city_to');
      table.integer('city_from');
      table.integer('customers_id');
      table.date('to_date').nullable();
      table.text('address_to').nullable();
      table.text('address_from').nullable();
      table.integer('transport_type').default(0);
      table.integer('max_space').default(0);
      
    })
  }

  down () {
    this.drop('trips')
  }
}

module.exports = TripsSchema
