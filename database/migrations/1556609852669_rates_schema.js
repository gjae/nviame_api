'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RatesSchema extends Schema {
  up () {
    this.create('rates', (table) => {
      table.increments()
      table.timestamps()
      table.date('deleted_at').nullable();
      table.integer('fixed_price').nullable().default(0);
      table.integer('per_km').nullable().default(0);
      table.integer('surplus_km').nullable().default(0);
      table.integer('customers_id');
    })
  }

  down () {
    this.drop('rates')
  }
}

module.exports = RatesSchema
