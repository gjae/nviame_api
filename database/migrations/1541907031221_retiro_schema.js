'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RetiroSchema extends Schema {
  up () {
    this.create('retiros', (table) => {
      table.increments()
      table.timestamps()
      table.dateTime("deleted_at").nullable();
      table.integer('movimiento_id').unsigned().notNullable();
      table.enu('estado_retiro', ['SOLICITADO', 'PROCESANDO', 'TRANSFERIDO']).defaultTo("SOLICITADO");
      table.string('cuenta_bancaria').nullable();
      table.string("banco").nullable();
    })
  }

  down () {
    this.drop('retiros')
  }
}

module.exports = RetiroSchema
