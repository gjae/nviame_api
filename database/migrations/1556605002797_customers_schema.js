'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.timestamps()
      table.date('birthday').nullable();
      table.datetime('verify_at').nullable();

      table.string("name", 200).nullable();
      table.string('lastname', 150).nullable();
      table.string('password', 32).nullable();
      table.string('identification', 30).nullable();
      table.string('email', 200).nullable();
      table.string('phone', 22).nullable();
      table.string('image', 100).nullable();
      table.string('facebook', 100).nullable();
      table.string('twitter', 100).nullable();
      table.string('zip_code', 9).default('00').index('ZCIND');
      table.string('address', 200).nullable();
      table.string('landline').nullable();
      table.string('socket_id').nullable();
      table.string('cuil', 100).nullable();
      table.string('verify_pin', 20).nullable();

      table.integer('city_id').default(0);
      table.integer('job').default(0);
      table.integer('state_id').default(0);
      table.integer('country_id').default(0);

    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomersSchema
