'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShipmentsSchema extends Schema {
  up () {
    this.create('shipments', (table) => {
      table.increments('id_shipment')
      table.timestamps()
      table.datetime('deleted_at').nullable();
      table.string('address_from').nullable();
      table.string('pin', 12).default('NPN').index('PNIND');

      table.string('address_to').nullable();
      table.integer('state').default(0);
      table.text('package_description').nullable();
      table.integer('customers_id');
      table.integer('category_id');
      table.integer('type_shipment_id');
      table.boolean('urgency').default(0);
      table.decimal('price').default(0.00);
      table.string('picture', 200).nullable();
      table.decimal('width').default(0.00);
      table.decimal('height').default(0.00);
      table.decimal('weight').default(0.00);
      table.decimal('depth').default(0.00);

    })
  }

  down () {
    this.drop('shipments')
  }
}

module.exports = ShipmentsSchema
