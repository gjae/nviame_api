'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BrandsSchema extends Schema {
  up () {
    this.create('brands', (table) => {
      table.increments()
      table.timestamps()
      table.string('name').notNull();
      table.string('brand_code', 9).default('BNC00').index('BNIND');
    })
  }

  down () {
    this.drop('brands')
  }
}

module.exports = BrandsSchema
