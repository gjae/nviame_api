'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BrandModelsSchema extends Schema {
  up () {
    this.create('brand_models', (table) => {
      table.increments('brand_model_id')
      table.timestamps()
      table.string('name').nullable();
      table.string('model').nullable();
      table.string('year', 4).nullable();
    })
  }

  down () {
    this.drop('brand_models')
  }
}

module.exports = BrandModelsSchema
