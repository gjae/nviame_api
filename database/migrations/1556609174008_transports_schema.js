'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TransportsSchema extends Schema {
  up () {
    this.create('transports', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 200).notNull();
      table.string('transport_code', 9).default('TC00').index('TCIND');
    })
  }

  down () {
    this.drop('transports')
  }
}

module.exports = TransportsSchema
