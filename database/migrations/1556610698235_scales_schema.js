'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ScalesSchema extends Schema {
  up () {
    this.create('scales', (table) => {
      table.increments()
      table.timestamps()
      table.integer('trip_id');
      table.integer('city_id');
      table.string('address', 200).nullable();
      table.float('lat').default(0);
      table.float('long').default(0);
      table.text('placeid').nullable();
    })
  }

  down () {
    this.drop('scales')
  }
}

module.exports = ScalesSchema
