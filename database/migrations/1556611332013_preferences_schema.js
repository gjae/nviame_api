'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PreferencesSchema extends Schema {
  up () {
    this.create('preferences', (table) => {
      table.increments()
      table.timestamps()
      table.integer('customers_id');
      table.integer('fixed_price').default(0);
      table.integer('rate_per_km').default(0);
    })
  }

  down () {
    this.drop('preferences')
  }
}

module.exports = PreferencesSchema
