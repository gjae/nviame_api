'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RatingsSchema extends Schema {
  up () {
    this.create('ratings', (table) => {
      table.increments()
      table.timestamps()
      table.datetime('date_added').notNull();
      table.integer('stars').notNull();
      table.text('comments').nullable();
      table.integer('to_customers_id').unsigned();
      table.integer('from_customers_id').unsigned();
    })
  }

  down () {
    this.drop('ratings')
  }
}

module.exports = RatingsSchema
