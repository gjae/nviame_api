'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersMovimientosSchema extends Schema {
  up () {
    this.create('customers_movimientos', (table) => {
      table.increments()
      table.timestamps()
      table.integer('customer_id');
      table.date('deleted_at').nullable();
      table.decimal('monto').default(0.00);
      table.string('tipo', 1).default('C');
      table.string('nro_referencia', 50).default('NR0000').index('NDIND');
      table.integer('shipment_id').default(0);
    })
  }

  down () {
    this.drop('customers_movimientos')
  }
}

module.exports = CustomersMovimientosSchema
