'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OffersSchema extends Schema {
  up () {
    this.create('offers', (table) => {
      table.increments()
      table.timestamps()
      table.integer('shipments_id');
      table.integer('customers_id');
      table.decimal('price_offer').default(0.00);
      table.integer('type_offer').nullable();
    })
  }

  down () {
    this.drop('offers')
  }
}

module.exports = OffersSchema
