'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PreferencesTransportsSchema extends Schema {
  up () {
    this.create('preferences_transports', (table) => {
      table.increments()
      table.timestamps()
      table.integer('preferences_id').default(0);
      table.integer('transports_id').default(0);
      table.datetime('actived_at').nullable();
      table.integer('status').default(0);
    })
  }

  down () {
    this.drop('preferences_transports')
  }
}

module.exports = PreferencesTransportsSchema
