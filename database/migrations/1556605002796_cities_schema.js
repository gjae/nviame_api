'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CitiesSchema extends Schema {
  up () {
    this.create('cities', (table) => {
      table.increments()
      table.timestamps()
      table.string('city').notNull();
      table.string('city_code', 10).default('C00').index('CCIND');
    })
  }

  down () {
    this.drop('cities')
  }
}

module.exports = CitiesSchema
