'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JobsSchema extends Schema {
  up () {
    this.create('jobs', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 150).notNull();
      table.string('code', 120).notNull();
      table.datetime('deleted_at');
    })
  }

  down () {
    this.drop('jobs')
  }
}

module.exports = JobsSchema
