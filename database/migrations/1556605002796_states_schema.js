'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatesSchema extends Schema {
  up () {
    this.create('states', (table) => {
      table.increments()
      table.timestamps()
      table.string('state').notNull();
      table.string('state_code', 10).default('ST00').index('STIND');
      table.integer('country_id').nullable();
    })
  }

  down () {
    this.drop('states')
  }
}

module.exports = StatesSchema
