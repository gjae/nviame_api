'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MaxSpacesSchema extends Schema {
  up () {
    this.create('max_spaces', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 150).notNull();
      table.integer('languages_id').nullable();
    })
  }

  down () {
    this.drop('max_spaces')
  }
}

module.exports = MaxSpacesSchema
