'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {
    this.create('chats', (table) => {
      table.increments()
      table.timestamps()
      table.datetime('date_deletion_customers_id').nullable();
      table.datetime('date_deletion_to_customers_id').nullable();
      table.integer('customers_id').default(0);
      table.integer('to_customers_id').default(0);
      table.text('text').default('');
      table.datetime('date_added').nullable();
    })
  }

  down () {
    this.drop('chats')
  }
}

module.exports = ChatSchema
