'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehiclesSchema extends Schema {
  up () {
    this.create('vehicles', (table) => {
      table.increments()
      table.timestamps()
      table.date('deleted_at').nullable();
      table.string('license_plat', 23).default('LPT00').index('LCPT');
      table.integer('brand_model_id');
      table.integer('customers_id');
      table.string('main_picture', 140).nullable();
      table.string('auth_card_picture', 130).nullable();
      table.string('insurence_picture', 130).nullable();
    })
  }

  down () {
    this.drop('vehicles')
  }
}

module.exports = VehiclesSchema
