const Serv = use('Server');
const io = use('socket.io')(Serv.getInstance());
const Customer = use('App/Models/Customer');
const Chat = use('App/Models/Chat');
const moment = require('moment');
const Tracking = use('App/Models/Traking');
const Trip = use('App/Models/Trip');
const Shipment = use('App/Models/Shipment');
const Rating = use('App/Models/Rating');

const SocketCont = use('App/Controllers/Ws/v1/SocketServerController');


var ServerSocket =  new SocketCont(io, null);



io.on('connection', function(socket){

	// ESTE EVENTO ES EMITIDO SIEMPRE QUE EL SOCKET SE RECONECTE
	// PARA QUE EL CLIENTE PUEDA VOLVER A ENVIAR LOS DATOS DEL USUARIO CONECTADO
	// Y SE REACTIVE LA TRANSFERENCIA DE DATOS DE MANERA AUTOMATICA
	socket.emit('connected', {has_error: false });

	/**
	*	EVENTOS PARA REGUSTRAR EL ID DEL SOCKET Y DESCONEXION DEL SOCKET
	*/

	// BANDERA PARA REALIZAR EL CORTE DEL setInterval PARA EL SOCKET CONECTADO, POR DEFECTO INICIALIZADA EN NULL
	var checkInterval = null;
	var intervalTracking = null;
	socket.on('data_user', async function(data){
		var customer = await Customer.query().where('id', '=', data.user_id).update({ socket_id: data.socket_id });


 		var messages = [];
		if( data.user_id != undefined ){

			// SE GENERA UN LOOP 
			checkInterval =  setInterval( async function(){
				var messenger_count = await Chat
				.query()
				.where('to_customers_id', '=', data.user_id)
				.whereNull('date_seen_to_customers_id')
				.count(' * as messenger_count ').first();

				console.log("MENSAJES PARA: "+socket.id);

				socket.emit('home_data', { ...messenger_count });
			}, 5000);

			/*messages = await Chat.query().where('to_customers_id', '=', data.user_id).whereNull('date_seen_to_customers_id');
			await Chat.query().where('to_customers_id', '=', data.user_id)
			.whereNull('date_seen_to_customers_id')
			.update({ date_seen_to_customers_id: "2018-08-20 00:00:00" });*/
		}

		//socket.emit('not_seen_messages', { messages  });
	} );

	socket.on('disconnect', async function(data){
		await Customer.query().where('socket_id' ,'=', socket.id).update({ socket_id: null });
		console.log("SE HA DESCONECTADO EL SOCKET: "+socket.id);
		clearInterval( checkInterval );
		clearInterval( intervalTracking );
	});

	/**
	*	EVENTOS PARA LA MENSAJERIA
	*/

	socket.on("my_messages", async function(data){
		// BUSCA EL USUARIO SEGUN LOS PARAMETROS DEL SOCKET
		var customer = await Customer.query().where('id', '=', data.user_id).first();
		var messages = null;

		if( customer ){

			// BUSCA TODOS LOS MENSAJES SIN LEER PARA ESE USUARIO
			messages = await Chat.query().where('to_customers_id', '=', customer.id)
			.with('from')
			.orderBy('chat.id', 'DESC')
			.fetch();

			messages = messages.toJSON();
			var chats = [];
			var buscados = [];
			var i = 0;
			for( var index in messages ){

				if( buscados.indexOf( messages[ index ].from.id ) == -1 ){
					var last_message = await Chat.query()
					.where( query => query.where('to_customers_id', '=', customer.id).where('customers_id', '=', messages[ index ].from.id ) )
					.orWhere( query => query.where('customers_id', '=', customer.id).where('to_customers_id', '=', messages[ index ].from.id ) )
					.orderBy( 'id', 'DESC' );


					var unseen = await Chat.query().where('to_customers_id', '=', customer.id)
					.where('customers_id', '=', messages[ index ].from.id )
					.whereNull('date_seen_to_customers_id').orderBy( 'id', 'DESC' );

					var trip = await Trip.query().where('customers_id', '=', customer.id)
					.with('transport')
					.where( query => query.where('from_date', '<=', moment( new Date ).format('YYYY-MM-DD  HH:MM:SS') ) )
					.orderBy('id_trip', 'DESC').first();

					if( last_message && last_message.length > 0 ){

						buscados.push( messages[ index ].from.id )
						chats[i] = {
							customer: {
								id: messages[index].from.id,
								name: messages[index].from.name,
								lastname: messages[ index ].from.lastname,
								image: messages[ index ].from.image,
								status: ( messages[ index ].from.socket_id == null ) ? 1 : 0
							},
							last_message: {
								date_time: last_message[0].date_added,
								seen: ( last_message[0].date_seen_to_customers_id == null ) ? 0 : 1,
								text: last_message[0].text
							},
							unseen_messages_count: unseen.length,
							trip_info:  trip

						};
						i++;
					} // FIN DE LA VALIDACION INTERNA
				} // FIN DE LA VALIDACION EXTERNA
			} // FIN DEL CICLO

		}

			// EL ATRIBUTO ONLY DEPRESENTA EL ID DEL USUARIO DEL CUAL SE A RECUPERADO LOS 10 ULTIMOS MENSAJES DEL CHAT
		var page = null;
		var perPage = null;
		if( data.only != undefined && messages != null ){
			page = ( data.page == undefined ) ? 1 : data.page;
			perPage = ( data.perPage == undefined ) ? 10 : data.perPage;

			// BUSCA TODOS LOS MENSAJES DE UN CHAT DPNDE EL USUARIO CONECTADO SEA EL QUE ENVIO O RECIBIO MMENSAJES DE UN
			// USUARIO ESPECIFICO ( DADO POR EL ATRIBUTO data.only EL CUAL ES EL ID DE LA PERSONA CON LA QUE ESTA CHATEANDO )
			// LOS TIENE DE FORMA DECRECIENTE POR SU ID PARA TENERLOS DE MODO QUE EL ULTIMO MENSAJE ENVIADO, SE VUELVA EL PRIMERO
			// DEL RESULTADO 
			var messages = await Chat.query()
			.where(query => query.where('to_customers_id', '=', customer.id).where('customers_id', '=', data.only) )
			.orWhere(query =>  query.where('to_customers_id', '=', data.only ).where('customers_id', '=', customer.id) )
			.with('from')
			.with("to")
			.orderBy('id', 'DESC')
			.offset(  ( page * perPage ) - perPage  )
			.limit(  perPage )
			.fetch();

			messages = messages.toJSON();

			var chats = [];
			var index = 0;
			for( var i = messages.length ; i >= 0 ; i-- ){
				console.log("entro en el ciclo "+messages.length);
				if( messages[i] != null ){
					chats[index] = {
						customer: {
							id: messages[i].from.id,
							name: messages[i].from.name,
							lastname: messages[i].from.name,
							image: messages[i].from.image
						},
						message:{
							id: messages[i].id,
							date_time: moment(messages[i].date_added, moment.ISO_8601),
							text: messages[i].text
						},
						type: ( messages[i].from.id == customer.id ) ? 'SENDER' : 'RECEIVER',
					};
					index++;
				}
			}

			// CAMBIA TODOS LOS MENSAJES PARA ESA PERSONA (ENVIADOS POR UNA PERSONA ESPECIFICA)
			// A VISTOS
			await Chat.query().where('to_customers_id', '=', customer.id)
			.where('customers_id', '=', data.only).update({ date_seen_to_customers_id: moment( new Date() ).format('YYYY-MM-DD hh:mm:ss') });
		}

		socket.emit('my_messages', { messages: chats, page, perPage, test: messages });

	});

	// EVENTO PARA GUARDAR UN MENSAJE DE UN CHAT ENTRE DOS PERSONAS
	socket.on('message', async function(data){
		console.log(data)

		// GUARDA EL MENSAJE
		var message = await Chat.create({
			customers_id: data.message.customers_id,
			to_customers_id: data.message.to_customers_id,
			text: data.message.text,
			date_added: moment( new Date() ).format('YYYY-MM-DD hh:mm:ss')
		});

		var to_customer = await Customer.query().where('id', '=', data.message.to_customers_id).first();
		var from_customer = await Customer.query().where('id', '=', data.message.customers_id).first();
		message = await Chat.query().where('id', '=', message.id).first();
		var to_socket = ( to_customer != null && to_customer.socket_id != null ) ? to_customer.socket_id : null;
		var message_id = message.id;
		if( message ){
			message = {
				customer: {
					id: data.message.customers_id,
					name: from_customer.name,
					lastname: from_customer.lastname,
					image: from_customer.image,
					status: (from_customer.socket_id == null) ? 0 : 1
				},
				last_message: {
					id: message.id,
					date_time: message.date_added,
					text: message.text
				}
			}
			message.type = "SENDER";
			message.tms = data.message.tms;
			socket.emit('message', { message });	
			if( to_socket != null ){ 
				message.type = "RECEIVER";
				io.to(to_socket).emit('message', { message });
				await Chat.query().where('id', '=', message_id)
				.update({ date_seen_to_customers_id: moment( new Date() ).format('YYYY-MM-DD hh:mm:ss') });
			}
		} 
	});

	/*
	*	EVENTOS PARA EL TRACKING
	*/

	//EVENTOS PARA TRABAJAR LA GEOLOCALIZACION Y EL TRACKING

	socket.on('tracking', async function(data){
		var tracking = await Tracking.create(data);
		if( tracking ){
			socket.emit('tracking', {error: false, tracking});
		}
	});

	socket.on( 'get_tracking', async function(data){

		intervalTracking = setInterval( async function(){
			// COORDENADAS
			var trackings = await Tracking.query().where('shipment_id', '=', data.shipment_id);
			
			// DATOS DEL VIAJERO
			var customerTraveler = await Customer.query()
			.where('id', '=', trackings[0].customer_id).select('name', 'lastname', 'id', 'image', 'socket_id')
			.with(
				'preferences', query => query.with(
						'transports', query => query.whereNotNull('actived_at')
					) 
			)
			.with('rates')
			.with('vehicles')
			.first();

		    var rating = await Rating.query().where('to_customers_id', '=', customerTraveler.id).sum('stars as stars').first();
		    var ratings = await Rating.query().where('to_customers_id', '=', customerTraveler.id).count('id as ids').first();

		    rating = rating.stars;
		    ratings = ratings.ids;			
		    customerTraveler.rating = ( rating == null || ratings == 0 ) ? 0 : rating / ratings;


			// CREADDOR DEL ENVIO

			console.log("ENVIANDO COORDENADAS")
			socket.emit('get_tracking', { trackings, traveler: customerTraveler });			
		}, 10000 )

	});

	//FIN DE LOS EVENTOS DEL TRACKCING
} );
