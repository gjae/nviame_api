'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/


const Route = use('Route');
const Env = use('Env');
const Serv = use('Server');


Route.get('/', ({response})=>{
	return response.status(200).send({ status: 'activo' });
});

Route.group( () =>{


	Route.get('testing', function({view}){

		return view.render('welcome2');
	});
	Route.get('chat3', function({view}){

		return view.render('welcome3');
	});

	Route.put('offers/accepted/:id', 'v1/OfferssController.accepted');
	
	Route.post('login', 'v1/CustomerController.login');
	Route.post('customer/register', 'v1/CustomerController.store' );
	Route.post('customer/verify', 'v1/CustomerController.verify' );
	Route.post('customer/restore/mail', 'v1/CustomerController.restore_mail' );
	Route.get('customer/restore/verify/:pin', 'v1/CustomerController.verify_code');
	Route.put('customer/restore/password', 'v1/CustomerController.reset');

	Route.resource('ratings', 'v1/RatingController').middleware('auth');


	Route.post('customer/socials/register' , 'v1/FacebookController.store');
	Route.post('customer/socials/login' , 'v1/FacebookController.show');

	Route.resource('vehicle_brands', 'v1/VehicleBrandController');
	Route.resource('countries', 'v1/CountryController');
	Route.resource('wallet', 'v1/WalletController').middleware('auth');
	Route.post('wallet/retiro', 'v1/WalletController.retiro').middleware('auth');

	Route.get('vehicles/types', 'v1/VehicleController.types');
	Route.get('shipments/categories', 'v1/ShipmentController.categories');
	Route.get('customer/jobs', 'v1/CustomerController.jobs').middleware('auth');
	Route.resource('spaces', 'v1/MaxSpaceController');
	Route.get('shipments/types', 'v1/ShipmentController.types');

	Route.put('trips/scales/:id', 'v1/TripController.updatScales').middleware('auth');
	Route.delete('trips/scales/:id', 'v1/TripController.deletedScales').middleware('auth');


	Route.put('offers/new_status/:id', 'v1/OfferssController.updateState').middleware('auth');

	Route.put('offers/:id', 'v1/OfferssController.update');

	Route.resource('trips', 'v1/TripController').middleware('auth');
	Route.resource('preferences', 'v1/PreferenceController').middleware('auth');
	Route.resource('/delivery', 'v1/DeliveryController').middleware('auth');
	Route.resource('customer', 'v1/CustomerController').middleware('auth');
	Route.resource('shipments', 'v1/ShipmentController').middleware('auth');
	Route.resource('offers', 'v1/OfferssController').middleware('auth');
	Route.resource('vehicles', 'v1/VehicleController').middleware('auth');
	
	Route.resource('countries', 'v1/CountryController');
	Route.resource('rates', 'v1/RatessController').middleware('auth');

	Route.get('chat', function({view}){
		return view.render('welcome');

	});


}).prefix('v1');
