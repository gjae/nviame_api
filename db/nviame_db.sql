-- MySQL dump 10.13  Distrib 5.7.21, for Linux (i686)
--
-- Host: nviame.com    Database: nviameco_db
-- ------------------------------------------------------
-- Server version	5.5.58-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beta_testing`
--

DROP TABLE IF EXISTS `beta_testing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beta_testing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beta_testing`
--

LOCK TABLES `beta_testing` WRITE;
/*!40000 ALTER TABLE `beta_testing` DISABLE KEYS */;
INSERT INTO `beta_testing` VALUES (6,'alejjo_89@hotmail.com'),(3,'marsicoviviana@hotmail.com'),(4,'federicozaia@gmail.com'),(5,'glarribite@gmail.com'),(7,'sjlopezorlandi@hotmail.com'),(8,'juankayer@live.com.ar'),(9,'magonandy@gmail.com');
/*!40000 ALTER TABLE `beta_testing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Calzado e Indumentaria'),(2,'Notebooks, Laptops'),(3,'Electrodomésticos'),(4,'Documentación'),(5,'Animales domésticos'),(6,'Mudanza'),(7,'Servicio de Flete'),(8,'Retiro de productos');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `to_customers_id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_seen_customers_id` datetime DEFAULT NULL,
  `date_seen_to_customers_id` datetime DEFAULT NULL,
  `date_deletion_customers_id` datetime DEFAULT NULL,
  `date_deletion_to_customers_id` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,2,1,'Hola Carlos!','2018-03-13 19:50:11',NULL,NULL,NULL,NULL),(2,3,1,'Voy camino a tu casa.','2018-03-13 19:58:02',NULL,NULL,NULL,NULL),(3,3,1,'Espérame listo, ok?','2018-03-13 19:59:17',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placeId` text NOT NULL,
  `city_name` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (4,'ChIJvQIT3Ku87ZUREXoeJI3Y0XY','Bahía Blanca','-38.7183177','-62.2663478'),(5,'ChIJvQz5TjvKvJURh47oiC6Bs6A','CABA','-34.6156625','-58.503338'),(6,'ChIJh2DKy5BSjZURivyuUo2VUGI','Monte Hermoso','-38.9805623','-61.3135955'),(7,'ChIJT7LKHeJ37ZURTzO5g4vZfpw','Punta Alta','-38.8719714','-62.0866408'),(8,'ChIJfxDBFVert5UR4wpxD5_WXis','Rosario','-32.9521399','-60.7681973');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_customers_id` int(11) NOT NULL,
  `to_customers_id` int(11) NOT NULL,
  `shipments_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,2,1,'Excelente.'),(2,1,2,2,'Todo en condiciones. Gracias.'),(3,3,1,3,'Gracias por la rapidez.');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` smallint(3) unsigned zerofill NOT NULL,
  `iso2` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `iso3` char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prefix` smallint(5) unsigned NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `continent` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcontinent` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_iso` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso2` (`iso2`),
  UNIQUE KEY `iso3` (`iso3`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,004,'AF','AFG',93,'Afganistán','Asia',NULL,'AFN','Afgani afgano'),(2,008,'AL','ALB',355,'Albania','Europa',NULL,'ALL','Lek albanés'),(3,010,'AQ','ATA',672,'Antártida','Antártida',NULL,NULL,NULL),(4,012,'DZ','DZA',213,'Argelia','África',NULL,'DZD','Dinar algerino'),(5,016,'AS','ASM',1684,'Samoa Americana','Oceanía',NULL,NULL,NULL),(6,020,'AD','AND',376,'Andorra','Europa',NULL,'EUR','Euro'),(7,024,'AO','AGO',244,'Angola','África',NULL,'AOA','Kwanza angoleño'),(8,028,'AG','ATG',1268,'Antigua y Barbuda','América','El Caribe',NULL,NULL),(9,031,'AZ','AZE',994,'Azerbaiyán','Asia',NULL,'AZM','Manat azerbaiyano'),(10,032,'AR','ARG',54,'Argentina','América','América del Sur','ARS','Peso argentino'),(11,036,'AU','AUS',61,'Australia','Oceanía',NULL,'AUD','Dólar australiano'),(12,040,'AT','AUT',43,'Austria','Europa',NULL,'EUR','Euro'),(13,044,'BS','BHS',1242,'Bahamas','América','El Caribe','BSD','Dólar bahameño'),(14,048,'BH','BHR',973,'Bahréin','Asia',NULL,'BHD','Dinar bahreiní'),(15,050,'BD','BGD',880,'Bangladesh','Asia',NULL,'BDT','Taka de Bangladesh'),(16,051,'AM','ARM',374,'Armenia','Asia',NULL,'AMD','Dram armenio'),(17,052,'BB','BRB',1246,'Barbados','América','El Caribe','BBD','Dólar de Barbados'),(18,056,'BE','BEL',32,'Bélgica','Europa',NULL,'EUR','Euro'),(19,060,'BM','BMU',1441,'Bermudas','América','El Caribe','BMD','Dólar de Bermuda'),(20,064,'BT','BTN',975,'Bhután','Asia',NULL,'BTN','Ngultrum de Bután'),(21,068,'BO','BOL',591,'Bolivia','América','América del Sur','BOB','Boliviano'),(22,070,'BA','BIH',387,'Bosnia y Herzegovina','Europa',NULL,'BAM','Marco convertible de Bosnia-Herzegovina'),(23,072,'BW','BWA',267,'Botsuana','África',NULL,'BWP','Pula de Botsuana'),(24,074,'BV','BVT',0,'Isla Bouvet',NULL,NULL,NULL,NULL),(25,076,'BR','BRA',55,'Brasil','América','América del Sur','BRL','Real brasileño'),(26,084,'BZ','BLZ',501,'Belice','América','América Central','BZD','Dólar de Belice'),(27,086,'IO','IOT',0,'Territorio Británico del Océano Índico',NULL,NULL,NULL,NULL),(28,090,'SB','SLB',677,'Islas Salomón','Oceanía',NULL,'SBD','Dólar de las Islas Salomón'),(29,092,'VG','VGB',1284,'Islas Vírgenes Británicas','América','El Caribe',NULL,NULL),(30,096,'BN','BRN',673,'Brunéi','Asia',NULL,'BND','Dólar de Brunéi'),(31,100,'BG','BGR',359,'Bulgaria','Europa',NULL,'BGN','Lev búlgaro'),(32,104,'MM','MMR',95,'Myanmar','Asia',NULL,'MMK','Kyat birmano'),(33,108,'BI','BDI',257,'Burundi','África',NULL,'BIF','Franco burundés'),(34,112,'BY','BLR',375,'Bielorrusia','Europa',NULL,'BYR','Rublo bielorruso'),(35,116,'KH','KHM',855,'Camboya','Asia',NULL,'KHR','Riel camboyano'),(36,120,'CM','CMR',237,'Camerún','África',NULL,NULL,NULL),(37,124,'CA','CAN',1,'Canadá','América','América del Norte','CAD','Dólar canadiense'),(38,132,'CV','CPV',238,'Cabo Verde','África',NULL,'CVE','Escudo caboverdiano'),(39,136,'KY','CYM',1345,'Islas Caimán','América','El Caribe','KYD','Dólar caimano (de Islas Caimán)'),(40,140,'CF','CAF',236,'República Centroafricana','África',NULL,NULL,NULL),(41,144,'LK','LKA',94,'Sri Lanka','Asia',NULL,'LKR','Rupia de Sri Lanka'),(42,148,'TD','TCD',235,'Chad','África',NULL,NULL,NULL),(43,152,'CL','CHL',56,'Chile','América','América del Sur','CLP','Peso chileno'),(44,156,'CN','CHN',86,'China','Asia',NULL,'CNY','Yuan Renminbi de China'),(45,158,'TW','TWN',886,'Taiwán','Asia',NULL,'TWD','Dólar taiwanés'),(46,162,'CX','CXR',61,'Isla de Navidad','Oceanía',NULL,NULL,NULL),(47,166,'CC','CCK',61,'Islas Cocos','Óceanía',NULL,NULL,NULL),(48,170,'CO','COL',57,'Colombia','América','América del Sur','COP','Peso colombiano'),(49,174,'KM','COM',269,'Comoras','África',NULL,'KMF','Franco comoriano (de Comoras)'),(50,175,'YT','MYT',262,'Mayotte','África',NULL,NULL,NULL),(51,178,'CG','COG',242,'Congo','África',NULL,NULL,NULL),(52,180,'CD','COD',243,'República Democrática del Congo','África',NULL,'CDF','Franco congoleño'),(53,184,'CK','COK',682,'Islas Cook','Oceanía',NULL,NULL,NULL),(54,188,'CR','CRI',506,'Costa Rica','América','América Central','CRC','Colón costarricense'),(55,191,'HR','HRV',385,'Croacia','Europa',NULL,'HRK','Kuna croata'),(56,192,'CU','CUB',53,'Cuba','América','El Caribe','CUP','Peso cubano'),(57,196,'CY','CYP',357,'Chipre','Europa',NULL,'CYP','Libra chipriota'),(58,203,'CZ','CZE',420,'República Checa','Europa',NULL,'CZK','Koruna checa'),(59,204,'BJ','BEN',229,'Benín','África',NULL,NULL,NULL),(60,208,'DK','DNK',45,'Dinamarca','Europa',NULL,'DKK','Corona danesa'),(61,212,'DM','DMA',1767,'Dominica','América','El Caribe',NULL,NULL),(62,214,'DO','DOM',1809,'República Dominicana','América','El Caribe','DOP','Peso dominicano'),(63,218,'EC','ECU',593,'Ecuador','América','América del Sur',NULL,NULL),(64,222,'SV','SLV',503,'El Salvador','América','América Central','SVC','Colón salvadoreño'),(65,226,'GQ','GNQ',240,'Guinea Ecuatorial','África',NULL,NULL,NULL),(66,231,'ET','ETH',251,'Etiopía','África',NULL,'ETB','Birr etíope'),(67,232,'ER','ERI',291,'Eritrea','África',NULL,'ERN','Nakfa eritreo'),(68,233,'EE','EST',372,'Estonia','Europa',NULL,'EEK','Corona estonia'),(69,234,'FO','FRO',298,'Islas Feroe','Europa',NULL,NULL,NULL),(70,238,'FK','FLK',500,'Islas Malvinas','América','América del Sur','FKP','Libra malvinense'),(71,239,'GS','SGS',0,'Islas Georgias del Sur y Sandwich del Sur','América','América del Sur',NULL,NULL),(72,242,'FJ','FJI',679,'Fiyi','Oceanía',NULL,'FJD','Dólar fijiano'),(73,246,'FI','FIN',358,'Finlandia','Europa',NULL,'EUR','Euro'),(74,248,'AX','ALA',0,'Islas Gland','Europa',NULL,NULL,NULL),(75,250,'FR','FRA',33,'Francia','Europa',NULL,'EUR','Euro'),(76,254,'GF','GUF',0,'Guayana Francesa','América','América del Sur',NULL,NULL),(77,258,'PF','PYF',689,'Polinesia Francesa','Oceanía',NULL,NULL,NULL),(78,260,'TF','ATF',0,'Territorios Australes Franceses',NULL,NULL,NULL,NULL),(79,262,'DJ','DJI',253,'Yibuti','África',NULL,'DJF','Franco yibutiano'),(80,266,'GA','GAB',241,'Gabón','África',NULL,NULL,NULL),(81,268,'GE','GEO',995,'Georgia','Europa',NULL,'GEL','Lari georgiano'),(82,270,'GM','GMB',220,'Gambia','África',NULL,'GMD','Dalasi gambiano'),(83,275,'PS','PSE',0,'Palestina','Asia',NULL,NULL,NULL),(84,276,'DE','DEU',49,'Alemania','Europa',NULL,'EUR','Euro'),(85,288,'GH','GHA',233,'Ghana','África',NULL,'GHC','Cedi ghanés'),(86,292,'GI','GIB',350,'Gibraltar','Europa',NULL,'GIP','Libra de Gibraltar'),(87,296,'KI','KIR',686,'Kiribati','Oceanía',NULL,NULL,NULL),(88,300,'GR','GRC',30,'Grecia','Europa',NULL,'EUR','Euro'),(89,304,'GL','GRL',299,'Groenlandia','América','América del Norte',NULL,NULL),(90,308,'GD','GRD',1473,'Granada','América','El Caribe',NULL,NULL),(91,312,'GP','GLP',0,'Guadalupe','América','El Caribe',NULL,NULL),(92,316,'GU','GUM',1671,'Guam','Oceanía',NULL,NULL,NULL),(93,320,'GT','GTM',502,'Guatemala','América','América Central','GTQ','Quetzal guatemalteco'),(94,324,'GN','GIN',224,'Guinea','África',NULL,'GNF','Franco guineano'),(95,328,'GY','GUY',592,'Guyana','América','América del Sur','GYD','Dólar guyanés'),(96,332,'HT','HTI',509,'Haití','América','El Caribe','HTG','Gourde haitiano'),(97,334,'HM','HMD',0,'Islas Heard y McDonald','Oceanía',NULL,NULL,NULL),(98,336,'VA','VAT',39,'Ciudad del Vaticano','Europa',NULL,NULL,NULL),(99,340,'HN','HND',504,'Honduras','América','América Central','HNL','Lempira hondureño'),(100,344,'HK','HKG',852,'Hong Kong','Asia',NULL,'HKD','Dólar de Hong Kong'),(101,348,'HU','HUN',36,'Hungría','Europa',NULL,'HUF','Forint húngaro'),(102,352,'IS','ISL',354,'Islandia','Europa',NULL,'ISK','Króna islandesa'),(103,356,'IN','IND',91,'India','Asia',NULL,'INR','Rupia india'),(104,360,'ID','IDN',62,'Indonesia','Asia',NULL,'IDR','Rupiah indonesia'),(105,364,'IR','IRN',98,'Irán','Asia',NULL,'IRR','Rial iraní'),(106,368,'IQ','IRQ',964,'Iraq','Asia',NULL,'IQD','Dinar iraquí'),(107,372,'IE','IRL',353,'Irlanda','Europa',NULL,'EUR','Euro'),(108,376,'IL','ISR',972,'Israel','Asia',NULL,'ILS','Nuevo shéquel israelí'),(109,380,'IT','ITA',39,'Italia','Europa',NULL,'EUR','Euro'),(110,384,'CI','CIV',225,'Costa de Marfil','África',NULL,NULL,NULL),(111,388,'JM','JAM',1876,'Jamaica','América','El Caribe','JMD','Dólar jamaicano'),(112,392,'JP','JPN',81,'Japón','Asia',NULL,'JPY','Yen japonés'),(113,398,'KZ','KAZ',7,'Kazajstán','Asia',NULL,'KZT','Tenge kazajo'),(114,400,'JO','JOR',962,'Jordania','Asia',NULL,'JOD','Dinar jordano'),(115,404,'KE','KEN',254,'Kenia','África',NULL,'KES','Chelín keniata'),(116,408,'KP','PRK',850,'Corea del Norte','Asia',NULL,'KPW','Won norcoreano'),(117,410,'KR','KOR',82,'Corea del Sur','Asia',NULL,'KRW','Won surcoreano'),(118,414,'KW','KWT',965,'Kuwait','Asia',NULL,'KWD','Dinar kuwaití'),(119,417,'KG','KGZ',996,'Kirguistán','Asia',NULL,'KGS','Som kirguís (de Kirguistán)'),(120,418,'LA','LAO',856,'Laos','Asia',NULL,'LAK','Kip lao'),(121,422,'LB','LBN',961,'Líbano','Asia',NULL,'LBP','Libra libanesa'),(122,426,'LS','LSO',266,'Lesotho','África',NULL,'LSL','Loti lesotense'),(123,428,'LV','LVA',371,'Letonia','Europa',NULL,'LVL','Lat letón'),(124,430,'LR','LBR',231,'Liberia','África',NULL,'LRD','Dólar liberiano'),(125,434,'LY','LBY',218,'Libia','África',NULL,'LYD','Dinar libio'),(126,438,'LI','LIE',423,'Liechtenstein','Europa',NULL,NULL,NULL),(127,440,'LT','LTU',370,'Lituania','Europa',NULL,'LTL','Litas lituano'),(128,442,'LU','LUX',352,'Luxemburgo','Europa',NULL,'EUR','Euro'),(129,446,'MO','MAC',853,'Macao','Asia',NULL,'MOP','Pataca de Macao'),(130,450,'MG','MDG',261,'Madagascar','África',NULL,'MGA','Ariary malgache'),(131,454,'MW','MWI',265,'Malaui','África',NULL,'MWK','Kwacha malauiano'),(132,458,'MY','MYS',60,'Malasia','Asia',NULL,'MYR','Ringgit malayo'),(133,462,'MV','MDV',960,'Maldivas','Asia',NULL,'MVR','Rufiyaa maldiva'),(134,466,'ML','MLI',223,'Malí','África',NULL,NULL,NULL),(135,470,'MT','MLT',356,'Malta','Europa',NULL,'MTL','Lira maltesa'),(136,474,'MQ','MTQ',0,'Martinica','América','El Caribe',NULL,NULL),(137,478,'MR','MRT',222,'Mauritania','África',NULL,'MRO','Ouguiya mauritana'),(138,480,'MU','MUS',230,'Mauricio','África',NULL,'MUR','Rupia mauricia'),(139,484,'MX','MEX',52,'México','América','América del Norte','MXN','Peso mexicano'),(140,492,'MC','MCO',377,'Mónaco','Europa',NULL,NULL,NULL),(141,496,'MN','MNG',976,'Mongolia','Asia',NULL,'MNT','Tughrik mongol'),(142,498,'MD','MDA',373,'Moldavia','Europa',NULL,'MDL','Leu moldavo'),(143,499,'ME','MNE',382,'Montenegro','Europa',NULL,NULL,NULL),(144,500,'MS','MSR',1664,'Montserrat','América','El Caribe',NULL,NULL),(145,504,'MA','MAR',212,'Marruecos','África',NULL,'MAD','Dirham marroquí'),(146,508,'MZ','MOZ',258,'Mozambique','África',NULL,'MZM','Metical mozambiqueño'),(147,512,'OM','OMN',968,'Omán','Asia',NULL,'OMR','Rial omaní'),(148,516,'NA','NAM',264,'Namibia','África',NULL,'NAD','Dólar namibio'),(149,520,'NR','NRU',674,'Nauru','Oceanía',NULL,NULL,NULL),(150,524,'NP','NPL',977,'Nepal','Asia',NULL,'NPR','Rupia nepalesa'),(151,528,'NL','NLD',31,'Países Bajos','Europa',NULL,'EUR','Euro'),(152,530,'AN','ANT',599,'Antillas Holandesas','América','El Caribe','ANG','Florín antillano neerlandés'),(153,533,'AW','ABW',297,'Aruba','América','El Caribe','AWG','Florín arubeño'),(154,540,'NC','NCL',687,'Nueva Caledonia','Oceanía',NULL,NULL,NULL),(155,548,'VU','VUT',678,'Vanuatu','Oceanía',NULL,'VUV','Vatu vanuatense'),(156,554,'NZ','NZL',64,'Nueva Zelanda','Oceanía',NULL,'NZD','Dólar neozelandés'),(157,558,'NI','NIC',505,'Nicaragua','América','América Central','NIO','Córdoba nicaragüense'),(158,562,'NE','NER',227,'Níger','África',NULL,NULL,NULL),(159,566,'NG','NGA',234,'Nigeria','África',NULL,'NGN','Naira nigeriana'),(160,570,'NU','NIU',683,'Niue','Oceanía',NULL,NULL,NULL),(161,574,'NF','NFK',0,'Isla Norfolk','Oceanía',NULL,NULL,NULL),(162,578,'NO','NOR',47,'Noruega','Europa',NULL,'NOK','Corona noruega'),(163,580,'MP','MNP',1670,'Islas Marianas del Norte','Oceanía',NULL,NULL,NULL),(164,581,'UM','UMI',0,'Islas Ultramarinas de Estados Unidos',NULL,NULL,NULL,NULL),(165,583,'FM','FSM',691,'Micronesia','Oceanía',NULL,NULL,NULL),(166,584,'MH','MHL',692,'Islas Marshall','Oceanía',NULL,NULL,NULL),(167,585,'PW','PLW',680,'Palaos','Oceanía',NULL,NULL,NULL),(168,586,'PK','PAK',92,'Pakistán','Asia',NULL,'PKR','Rupia pakistaní'),(169,591,'PA','PAN',507,'Panamá','América','América Central','PAB','Balboa panameña'),(170,598,'PG','PNG',675,'Papúa Nueva Guinea','Oceanía',NULL,'PGK','Kina de Papúa Nueva Guinea'),(171,600,'PY','PRY',595,'Paraguay','América','América del Sur','PYG','Guaraní paraguayo'),(172,604,'PE','PER',51,'Perú','América','América del Sur','PEN','Nuevo sol peruano'),(173,608,'PH','PHL',63,'Filipinas','Asia',NULL,'PHP','Peso filipino'),(174,612,'PN','PCN',870,'Islas Pitcairn','Oceanía',NULL,NULL,NULL),(175,616,'PL','POL',48,'Polonia','Europa',NULL,'PLN','zloty polaco'),(176,620,'PT','PRT',351,'Portugal','Europa',NULL,'EUR','Euro'),(177,624,'GW','GNB',245,'Guinea-Bissau','África',NULL,NULL,NULL),(178,626,'TL','TLS',670,'Timor Oriental','Asia',NULL,NULL,NULL),(179,630,'PR','PRI',1,'Puerto Rico','América','El Caribe',NULL,NULL),(180,634,'QA','QAT',974,'Qatar','Asia',NULL,'QAR','Rial qatarí'),(181,638,'RE','REU',262,'Reunión','África',NULL,NULL,NULL),(182,642,'RO','ROU',40,'Rumania','Europa',NULL,'RON','Leu rumano'),(183,643,'RU','RUS',7,'Rusia','Asia',NULL,'RUB','Rublo ruso'),(184,646,'RW','RWA',250,'Ruanda','África',NULL,'RWF','Franco ruandés'),(185,654,'SH','SHN',290,'Santa Helena','África',NULL,'SHP','Libra de Santa Helena'),(186,659,'KN','KNA',1869,'San Cristóbal y Nieves','América','El Caribe',NULL,NULL),(187,660,'AI','AIA',1264,'Anguila','América','El Caribe',NULL,NULL),(188,662,'LC','LCA',1758,'Santa Lucía','América','El Caribe',NULL,NULL),(189,666,'PM','SPM',508,'San Pedro y Miquelón','América','América del Norte',NULL,NULL),(190,670,'VC','VCT',1784,'San Vicente y las Granadinas','América','El Caribe',NULL,NULL),(191,674,'SM','SMR',378,'San Marino','Europa',NULL,NULL,NULL),(192,678,'ST','STP',239,'Santo Tomé y Príncipe','África',NULL,'STD','Dobra de Santo Tomé y Príncipe'),(193,682,'SA','SAU',966,'Arabia Saudí','Asia',NULL,'SAR','Riyal saudí'),(194,686,'SN','SEN',221,'Senegal','África',NULL,NULL,NULL),(195,688,'RS','SRB',381,'Serbia','Europa',NULL,NULL,NULL),(196,690,'SC','SYC',248,'Seychelles','África',NULL,'SCR','Rupia de Seychelles'),(197,694,'SL','SLE',232,'Sierra Leona','África',NULL,'SLL','Leone de Sierra Leona'),(198,702,'SG','SGP',65,'Singapur','Asia',NULL,'SGD','Dólar de Singapur'),(199,703,'SK','SVK',421,'Eslovaquia','Europa',NULL,'SKK','Corona eslovaca'),(200,704,'VN','VNM',84,'Vietnam','Asia',NULL,'VND','Dong vietnamita'),(201,705,'SI','SVN',386,'Eslovenia','Europa',NULL,NULL,NULL),(202,706,'SO','SOM',252,'Somalia','África',NULL,'SOS','Chelín somalí'),(203,710,'ZA','ZAF',27,'Sudáfrica','África',NULL,'ZAR','Rand sudafricano'),(204,716,'ZW','ZWE',263,'Zimbabue','África',NULL,'ZWL','Dólar zimbabuense'),(205,724,'ES','ESP',34,'España','Europa',NULL,'EUR','Euro'),(206,732,'EH','ESH',0,'Sahara Occidental','África',NULL,NULL,NULL),(207,736,'SD','SDN',249,'Sudán','África',NULL,'SDD','Dinar sudanés'),(208,740,'SR','SUR',597,'Surinam','América','América del Sur','SRD','Dólar surinamés'),(209,744,'SJ','SJM',0,'Svalbard y Jan Mayen','Europa',NULL,NULL,NULL),(210,748,'SZ','SWZ',268,'Suazilandia','África',NULL,'SZL','Lilangeni suazi'),(211,752,'SE','SWE',46,'Suecia','Europa',NULL,'SEK','Corona sueca'),(212,756,'CH','CHE',41,'Suiza','Europa',NULL,'CHF','Franco suizo'),(213,760,'SY','SYR',963,'Siria','Asia',NULL,'SYP','Libra siria'),(214,762,'TJ','TJK',992,'Tayikistán','Asia',NULL,'TJS','Somoni tayik (de Tayikistán)'),(215,764,'TH','THA',66,'Tailandia','Asia',NULL,'THB','Baht tailandés'),(216,768,'TG','TGO',228,'Togo','África',NULL,NULL,NULL),(217,772,'TK','TKL',690,'Tokelau','Oceanía',NULL,NULL,NULL),(218,776,'TO','TON',676,'Tonga','Oceanía',NULL,'TOP','Pa\'anga tongano'),(219,780,'TT','TTO',1868,'Trinidad y Tobago','América','El Caribe','TTD','Dólar de Trinidad y Tobago'),(220,784,'AE','ARE',971,'Emiratos Árabes Unidos','Asia',NULL,'AED','Dirham de los Emiratos Árabes Unidos'),(221,788,'TN','TUN',216,'Túnez','África',NULL,'TND','Dinar tunecino'),(222,792,'TR','TUR',90,'Turquía','Asia',NULL,'TRY','Lira turca'),(223,795,'TM','TKM',993,'Turkmenistán','Asia',NULL,'TMM','Manat turcomano'),(224,796,'TC','TCA',1649,'Islas Turcas y Caicos','América','El Caribe',NULL,NULL),(225,798,'TV','TUV',688,'Tuvalu','Oceanía',NULL,NULL,NULL),(226,800,'UG','UGA',256,'Uganda','África',NULL,'UGX','Chelín ugandés'),(227,804,'UA','UKR',380,'Ucrania','Europa',NULL,'UAH','Grivna ucraniana'),(228,807,'MK','MKD',389,'Macedonia','Europa',NULL,'MKD','Denar macedonio'),(229,818,'EG','EGY',20,'Egipto','África',NULL,'EGP','Libra egipcia'),(230,826,'GB','GBR',44,'Reino Unido','Europa',NULL,'GBP','Libra esterlina (libra de Gran Bretaña)'),(231,834,'TZ','TZA',255,'Tanzania','África',NULL,'TZS','Chelín tanzano'),(232,840,'US','USA',1,'Estados Unidos','América','América del Norte','USD','Dólar estadounidense'),(233,850,'VI','VIR',1340,'Islas Vírgenes de los Estados Unidos','América','El Caribe',NULL,NULL),(234,854,'BF','BFA',226,'Burkina Faso','África',NULL,NULL,NULL),(235,858,'UY','URY',598,'Uruguay','América','América del Sur','UYU','Peso uruguayo'),(236,860,'UZ','UZB',998,'Uzbekistán','Asia',NULL,'UZS','Som uzbeko'),(237,862,'VE','VEN',58,'Venezuela','América','América del Sur','VEF','Bolívar fuerte'),(238,876,'WF','WLF',681,'Wallis y Futuna','Oceanía',NULL,NULL,NULL),(239,882,'WS','WSM',685,'Samoa','Oceanía',NULL,'WST','Tala samoana'),(241,894,'ZM','ZMB',260,'Zambia','África',NULL,'ZMK','Kwacha zambiano');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `lastname` text NOT NULL,
  `identification` int(8) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` text NOT NULL,
  `birthday` datetime NOT NULL,
  `city_id` int(2) NOT NULL,
  `state_id` int(3) NOT NULL,
  `country_id` int(3) NOT NULL,
  `image` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `linkedin` text NOT NULL,
  `job` int(3) NOT NULL,
  `cuil` text NOT NULL,
  `rating` decimal(10,0) NOT NULL,
  `fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Carlos','Schmidt',32838616,'carlos.schmidt@live.com.ar','3ed10e3c83f24f251d0f813d045e216218309e37','2914767150','1985-05-26 08:30:00',1,2,10,'customers/carlos-schmidt-1.jpg','scharlestone','scharlestone','carlosschmidt',2,'20328386160',7,18),(2,'Andrea','Capdevila',32253322,'andy_capdevila@hotmail.com','3ed10e3c83f24f251d0f813d045e216218309e37','2914791611','1986-02-13 20:30:00',1,2,10,'customers/andrea-cadepvila-1.jpg','andrea.capdevila.7','','',2,'27322533220',8,18),(3,'Alejo','Triches',33221852,'alejjo.89@gmail.com','9fa9f8dd6f9bc509bdb39faa0ff30967038ac115','2915573214','1989-02-28 00:00:00',1,2,10,'customers/alejo-triches-3.jpg','alejjo.89','','',3,'20332218520',6,18);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers_facebook_sign_in`
--

DROP TABLE IF EXISTS `customers_facebook_sign_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers_facebook_sign_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `profile_id` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `cover` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers_facebook_sign_in`
--

LOCK TABLES `customers_facebook_sign_in` WRITE;
/*!40000 ALTER TABLE `customers_facebook_sign_in` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers_facebook_sign_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveries`
--

DROP TABLE IF EXISTS `deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveries` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `shipments_id` int(7) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveries`
--

LOCK TABLES `deliveries` WRITE;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
INSERT INTO `deliveries` VALUES (1,1,3,0,'2018-03-18 00:00:00','2018-03-20 00:00:00'),(2,2,3,1,'2018-03-01 00:00:00','2018-03-03 00:00:00');
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) NOT NULL,
  `customers_id_favorite` int(10) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
INSERT INTO `favorites` VALUES (1,1,2,'2018-03-02 01:48:41'),(2,2,1,'2018-03-18 00:00:00');
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `code` varchar(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'español','es-ES'),(2,'english','en-US');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `max_spaces`
--

DROP TABLE IF EXISTS `max_spaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `max_spaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `languages_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `max_spaces`
--

LOCK TABLES `max_spaces` WRITE;
/*!40000 ALTER TABLE `max_spaces` DISABLE KEYS */;
INSERT INTO `max_spaces` VALUES (1,'Maletín Notebook',1),(2,'Documentación',1),(3,'Mochila',1),(4,'Valija de mano',1),(5,'Valija mediana',1),(6,'Valija grande',1),(7,'Caja chica (30x20x10 cm) ',1),(8,'Caja mediana (50x30x30 cm) ',1),(9,'Caja grande (70x40x50 cm) ',1);
/*!40000 ALTER TABLE `max_spaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipments_id` int(6) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `to_customers_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `type_offer` int(2) NOT NULL COMMENT '1=fijo, 2=kilometro',
  `price_offer` decimal(10,0) NOT NULL,
  `accepted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,1,1,1,'2018-03-02 00:54:18',1,250,'2018-03-02 00:59:07'),(2,2,2,0,'2018-03-02 02:05:41',2,310,'2018-03-02 01:00:00'),(3,1,3,1,'2018-03-18 00:00:00',1,400,'2018-03-18 00:00:00'),(4,1,2,1,'2018-03-12 00:00:00',1,400,'2018-03-13 00:00:00');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferences`
--

DROP TABLE IF EXISTS `preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `fixed_price` int(1) NOT NULL COMMENT '0=no, 1=si',
  `deviation` int(1) NOT NULL COMMENT '0=no, 1=si',
  `charge_from` int(3) NOT NULL COMMENT 'En KM.',
  `charge_to` int(3) NOT NULL COMMENT 'En KM.',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferences`
--

LOCK TABLES `preferences` WRITE;
/*!40000 ALTER TABLE `preferences` DISABLE KEYS */;
INSERT INTO `preferences` VALUES (1,1,1,1,10,50),(3,2,0,0,1,100);
/*!40000 ALTER TABLE `preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferences_transports`
--

DROP TABLE IF EXISTS `preferences_transports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferences_transports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preferences_id` int(11) NOT NULL,
  `transports_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferences_transports`
--

LOCK TABLES `preferences_transports` WRITE;
/*!40000 ALTER TABLE `preferences_transports` DISABLE KEYS */;
INSERT INTO `preferences_transports` VALUES (1,3,1),(3,3,3),(4,1,1),(5,1,2),(6,1,3),(8,1,4);
/*!40000 ALTER TABLE `preferences_transports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `fixed_price` decimal(10,0) NOT NULL,
  `per_km` decimal(10,0) NOT NULL,
  `surplus_km` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rates`
--

LOCK TABLES `rates` WRITE;
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
INSERT INTO `rates` VALUES (1,1,100,100,100),(2,2,200,200,200);
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `from_customers_id` int(6) NOT NULL,
  `to_customers_id` int(6) NOT NULL,
  `stars` tinyint(1) NOT NULL,
  `comments` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,2,1,5,'Todo bien!','2018-03-02 11:44:09'),(6,3,1,4,'Bien','2018-03-02 12:57:19');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipments` (
  `id_shipment` int(5) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `placeID_from` text NOT NULL,
  `placeID_to` text NOT NULL,
  `address_from` varchar(200) NOT NULL,
  `lat_from` text NOT NULL,
  `long_from` text NOT NULL,
  `address_to` varchar(200) NOT NULL,
  `lat_to` text NOT NULL,
  `long_to` text NOT NULL,
  `category_id` int(3) NOT NULL DEFAULT '0',
  `picture` text NOT NULL,
  `package_description` varchar(150) NOT NULL,
  `height` int(4) NOT NULL DEFAULT '0',
  `width` int(4) NOT NULL DEFAULT '0',
  `depth` int(4) NOT NULL DEFAULT '0',
  `weight` int(4) NOT NULL DEFAULT '0',
  `ship_now` int(1) NOT NULL DEFAULT '1' COMMENT '1=si, 2=programar',
  `max_arrive_date` datetime DEFAULT NULL,
  `pick_up_point` int(11) NOT NULL DEFAULT '0' COMMENT '0=aqui,1=nuevo lugar',
  `pick_up_address` text NOT NULL,
  `payment_type` int(1) NOT NULL DEFAULT '0' COMMENT '0=pago fijo, 1=recibe ofertas',
  `price` int(11) NOT NULL DEFAULT '0',
  `urgency` int(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si',
  `pin` text NOT NULL,
  `state` int(1) NOT NULL COMMENT '0=no, 1=si, 2=problemas',
  PRIMARY KEY (`id_shipment`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipments`
--

LOCK TABLES `shipments` WRITE;
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` VALUES (2,1,'1','2','Saavedra 39','-38.7212927','-62.2712351','Lima 31','-34.6087939','-58.3831711',1,'shipments/zapatos.jpg','Par de zapatos Demócrata talle 42, color gris.',15,45,25,1,1,'2018-03-05 00:00:00',0,'',1,300,0,'1-2-22883',0),(1,1,'1','2','Palau 69 Piso 6','-38.724405','-62.2584865','Carapachay 3760','-34.521932','-58.5370123',4,'shipments/escritura.jpg','Escritura catastral Folio Nº 2221.',21,45,2,0,1,'2018-03-01 00:00:00',0,'',1,300,1,'2-4-33421',0);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `code` varchar(4) NOT NULL,
  `name` varchar(19) NOT NULL,
  `country_id` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (11,'AR-L','La Pampa',32),(12,'AR-M','Mendoza',32),(13,'AR-N','Misiones',32),(14,'AR-P','Formosa',32),(15,'AR-Q','Neuquén',32),(16,'AR-R','Río Negro',32),(17,'AR-S','Santa Fe',32),(18,'AR-T','Tucumán',32),(19,'AR-U','Chubut',32),(20,'AR-V','Tierra del Fuego',32),(21,'AR-W','Corrientes',32),(22,'AR-X','Córdoba',32),(23,'AR-Y','Jujuy',32),(24,'AR-Z','Santa Cruz',32),(1,'AR-A','Salta',32),(2,'AR-B','Buenos Aires',32),(3,'AR-C','Capital Federal',32),(4,'AR-D','San Luis',32),(5,'AR-E','Entre Ríos',32),(6,'AR-F','La Rioja',32),(7,'AR-G','Santiago del Estero',32),(8,'AR-H','Chaco',32),(9,'AR-J','San Juan',32),(10,'AR-K','Catamarca',32);
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Pendiente'),(2,'Recibe ofertas'),(3,'Oferta aceptada'),(4,'Esperando retiro'),(5,'Retiro realizado'),(6,'En camino'),(7,'Listo para entregar'),(8,'Entregado'),(9,'1er. intento de entrega'),(10,'2do. intento de entrega'),(11,'Agotados los intentos de entrega.'),(12,'En devolución'),(13,'Devuelto');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transports`
--

DROP TABLE IF EXISTS `transports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transports`
--

LOCK TABLES `transports` WRITE;
/*!40000 ALTER TABLE `transports` DISABLE KEYS */;
INSERT INTO `transports` VALUES (1,'Bicicleta'),(2,'Moto'),(3,'Auto'),(4,'Utilitario'),(5,'Camioneta'),(6,'Furgón'),(7,'Avión');
/*!40000 ALTER TABLE `transports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trips` (
  `id_trip` int(5) NOT NULL AUTO_INCREMENT,
  `customers_id` int(6) NOT NULL,
  `city_from` int(5) NOT NULL,
  `city_to` int(5) NOT NULL,
  `address_from` varchar(40) NOT NULL,
  `address_to` text NOT NULL,
  `stopover_city` int(5) NOT NULL,
  `stopover_address` varchar(40) NOT NULL,
  `transport_type` int(2) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `max_space` int(2) NOT NULL,
  PRIMARY KEY (`id_trip`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trips`
--

LOCK TABLES `trips` WRITE;
/*!40000 ALTER TABLE `trips` DISABLE KEYS */;
INSERT INTO `trips` VALUES (1,1,4,5,'Patricios 825','Mitre 4350',0,'0',7,'2018-03-02 00:00:00','2018-02-03 00:00:00',4),(2,2,5,4,'Av. Libertador 450','Brown 150',0,'0',7,'2018-03-01 00:00:00','2018-02-05 00:00:00',3),(13,3,4,6,'Salinas Chicas 918','Bahía Blanca 101',0,'0',1,'2018-03-20 00:00:00','2018-03-21 00:00:00',3),(14,3,4,7,'Salinas Chicas 918','Brown 420',0,'0',1,'2018-03-23 00:00:00','2018-03-25 00:00:00',3);
/*!40000 ALTER TABLE `trips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(10) NOT NULL,
  `main_picture` text NOT NULL,
  `driver_license_picture` text NOT NULL,
  `auth_card_picture` text NOT NULL,
  `insurance_picture` text NOT NULL,
  `transport_id` int(11) NOT NULL,
  `brand` text NOT NULL,
  `model` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `license_plate` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (1,1,'1_p106.jpg','1_lic.jpg','','1_seguro.jpg',1,'Peugeot',106,1998,'CEJ 974');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-20 18:57:13
