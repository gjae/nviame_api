'use strict'

const Model = use('Model')

class Job extends Model {

	static get table(){ return "jobs"; }
	static get hidden(){ return ['created_at', 'deleted_at', 'updated_at']; }

	customers(){
		return this.hasMany('App/Models/Customer', 'id', 'job');
	}
}

module.exports = Job
