'use strict'

const Model = use('Model')

class PrefTransp extends Model {
	static get table(){ return "preferences_transports"; }
}

module.exports = PrefTransp
