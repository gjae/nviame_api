'use strict'

const Model = use('Model')

class Shipment extends Model {
	static get table() { return 'shipments'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return ['pin', 'customers_id']; }
	static get primaryKey(){ return 'id_shipment'; }
	static get hidden(){ return [ 'category_id', 'customers_id' ]; }

	static validationRules(){  
		return {
			placeID_from: 'required',
			to: 'required',
			address_from: 'required',
			lat_from: 'required',
			lon_from: 'required',
			category_id: 'required',
			package_description: 'required',
			height: 'required',
			width: 'required',
			depth: 'required',
			weight: 'required',
			ship_now: 'required',
			max_arrive_date: 'required',
			pick_up_point: 'required',
			pick_up_address: 'required',
			payment_type: 'required',
			price: 'required',
			urgency: 'required',
		};	
	}

	customer(){
		return this.belongsTo( 'App/Models/Customer', 'customers_id', 'id' );
	}

	offers(){
		return this.hasMany( 'App/Models/Offer', 'id_shipment', 'shipments_id' );
	}

	category(){
		return this.belongsTo( 'App/Models/Category', 'category_id');
	}

	delivery(){
		return this.hasOne( 'App/Models/Delivery', 'id_shipment', 'shipments_id' );
	}

	type(){
		return this.belongsTo('App/Models/Typeshipmment', 'type_shipment_id', 'id');
	}
}

module.exports = Shipment
