'use strict'

const Model = use('Model')

class Typeshipment extends Model {

	static get table(){ return "type_shipments"; }
	static get hidden(){ return ['deleted_at', 'updated_at', 'created_at']; }
}

module.exports = Typeshipment
