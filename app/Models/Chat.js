'use strict'

const Model = use('Model')

class Chat extends Model {
	static get table() { return 'chat'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return [ 'date_deletion_customers_id', 'date_deletion_to_customers_id', 'customers_id', 'to_customers_id' ]; }

	from(){
		return this.belongsTo('App/Models/Customer', 'customers_id', 'id');
	}

	to(){
		return this.belongsTo( 'App/Models/Customer', 'to_customers_id', 'id');
	}
}

module.exports = Chat
