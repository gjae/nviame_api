'use strict'

const Model = use('Model');

/**
 * TEST TOKEN: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTUyNDI0Nzk3NH0.9uw665QFK5w8f3s0C9iLv0MM2zJEjJzU_2UCJb4dme8
 */
class Customer extends Model {

	static get table(){ return "customers"; }
	static get createdAtColumn(){ return null; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return ['password','state_id', 'city_id', 'country_id', 'verify_pin', 'verify_at']; }


	social_profile(){

		return this.hasMany( 'App/Models/Socialsig' , 'customer_id', 'id' );
	}


	city(){
		return this.belongsTo('App/Models/City', 'city_id', 'id');
	}

	state(){
		return this.belongsTo('App/Models/State');
	}

	country(){
		return this.belongsTo( 'App/Models/Country', 'country_id', 'id' );
	}

	vehicles(){
		return this.hasMany( 'App/Models/Country', 'customers_id',  'id' );
	}

	shipments(){
		return this.hasMany( 'App/Models/Shipment', 'id', 'customers_id' );
	}

	mycoments(){
		return this.hasMany( 'App/Models/Comment', 'id', 'from_customers_id' );
	}

	comentsToMe(){
		return this.hasMany( 'App/Models/Coment', 'id', 'to_customers_id' );
	}

	references(){
		return this.hasMany( 'App/Models/Rating', 'id', 'to_customers_id');
	}

	rates(){
		return this.hasOne('App/Models/Rate', 'id', 'customers_id');
	}

	my_ratings(){
		return this.hasMany('App/Models/Rating', 'id', 'to_customers_id');
	}

	from_ratings(){
		return this.hasMany('App/Models/Rating', 'id', 'from_customers_id');

	}

	jobs(){

		return this.belongsTo('App/Models/Job', 'job', 'id');
	}

	movimientos(){
		return this.hasMany( 'App/Models/Movimiento', 'customer_id', 'id' );
	}

	preferences(){
		return this.hasOne('App/Models/Preference', 'id', 'customers_id');
	}
}

module.exports = Customer
