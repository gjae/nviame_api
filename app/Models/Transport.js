'use strict'

const Model = use('Model')

class Transport extends Model {
	static get table() { return 'transports'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	vehicles(){
		return this.hasMany( 'App/Models/Vehicle' );
	}

	
}

module.exports = Transport
