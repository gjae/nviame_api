'use strict'

const Model = use('Model')

class Favority extends Model {
	static get table() { return 'favorites'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
}

module.exports = Favority
