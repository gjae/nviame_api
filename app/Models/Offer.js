'use strict'

const Model = use('Model')

class Offer extends Model {
	static get table() { return 'offers'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return ['deleted_at']; }

	static get dates(){
		return super.dates.concat(['deleted_at', 'accepted_date']);
	}

	shipment(){
		return this.belongsTo( 'App/Models/Shipment', 'shipments_id', 'id_shipment' );
	}

	customer_offer(){
		return this.belongsTo( 'App/Models/Customer', 'customers_id', 'id' );
	}
}

module.exports = Offer
