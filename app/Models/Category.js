'use strict'

const Model = use('Model')

class Category extends Model {
	static get table() { return 'categories'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	shipments(){
		return this.hasMany( 'App/Models/Shipment', 'id', 'category_id' );
	}
}

module.exports = Category
