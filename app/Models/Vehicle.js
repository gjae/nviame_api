'use strict'

const Model = use('Model')

class Vehicle extends Model {
	static get table() { return 'vehicles'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return ['deleted_at', 'transport_id', 'brand_model_id', 'brand_id']; }
	static get dates(){
		return super.dates.concat(['deleted_at']);
	}

	customer(){
		return this.belongsTo( 'App/Models/Customer', 'customers_id', 'id' );
	}

	transport(){
		return this.belongsTo( 'App/Models/Transport' );
	}

	brand(){
		return this.belongsTo('App/Models/Brand');
	}

	model(){
		return this.belongsTo('App/Models/Bmdl', 'brand_model_id', 'id');
	}

}

module.exports = Vehicle
