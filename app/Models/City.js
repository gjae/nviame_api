'use strict'

const Model = use('Model')
const customer = use('App/Models/Customer');

class City extends Model {
	static get table() { return 'cities'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	customers(){
		return this.hasMany('App/Models/Customer', 'id', 'city_id');
	}
}

module.exports = City
