'use strict'

const Model = use('Model')

class Bmdl extends Model {
	static get table(){ return 'brand_models'; }
	static get hidden(){ return ['created_at', 'deleted_at', 'updated_at']; }

	vehicles(){
		return this.hasMany('App/Models/Vehicle', 'id', 'brand_model_id');
	}
}

module.exports = Bmdl
