'use strict'

const Model = use('Model')


/**
*	POSIBLES STATUS DE UN DELIVERY:
*	1 - pendiente
*	6 - en camino / en viaje
*	4 - esperando retiro
*	5 - entregado
*/
class Delivery extends Model {
	static get table() { return 'deliveries'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	getStatus(status){
		switch(status){
			case 4: return 'Esperando retiro';
			case 5: return 'Retiro realizado';
			case 6: return 'En camino';
			case 7: return 'Listo para entregar';
			case 8: return 'Entregado';
			case 13: return 'Devuelto';
		}
	}

	shipment(){
		return this.belongsTo( 'App/Models/Shipment', 'shipments_id', 'id_shipment' );
	}

	customer(){
		return this.belongsTo( 'App/Models/Customer', 'customers_id', 'id');
	}
}

module.exports = Delivery
