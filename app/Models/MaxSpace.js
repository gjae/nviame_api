'use strict'

const Model = use('Model')

class MaxSpace extends Model {
	static get table(){ return 'max_spaces'; }
	static get createdAtColumn(){ return null; }
}

module.exports = MaxSpace
