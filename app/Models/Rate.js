'use strict'

const Model = use('Model')

class Rate extends Model {
	static get table() { return 'rates'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }
	static get hidden(){ return ['customers_id', 'deleted_at']; }

	customer(){
		return this.belongsTo( 'App/Models/Customer', 'customers_id', 'id');
	}
}

module.exports = Rate
