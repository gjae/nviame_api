'use strict'

const Model = use('Model')

class Rating extends Model {

	static get table(){ return 'ratings'; }
	static get cratedAtColumn(){ return null; }
	static get updatedAtColumn(){ return null; }
	static get hidden(){ return ['to_customers_id', 'from_customers_id']; }

	static get dates(){
		return super.dates.concat(['date_added']);
	}

	from(){
		return this.belongsTo( 'App/Models/Customer', 'from_customers_id', 'id' );
	}

	to(){
		return this.belongsTo( 'App/Models/Customer', 'to_customers_id', 'id' );
	}

}

module.exports = Rating
