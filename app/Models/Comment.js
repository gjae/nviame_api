'use strict'

const Model = use('Model')

class Comment extends Model {
	static get table() { return 'comments'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	from(){
		return this.belongsTo( 'App/Models/Customer', 'from_customers_id', 'id' );
	}
}

module.exports = Comment
