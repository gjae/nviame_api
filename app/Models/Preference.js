'use strict'

const Model = use('Model')

class Preference extends Model {
	static get table() { return 'preferences'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }


	transports(){
		return this.hasMany('App/Models/PrefTransp', 'id', 'preferences_id');
	}
}

module.exports = Preference
