'use strict'

const Model = use('Model')

class State extends Model {
	get table(){ return 'states'; }
	get createdAtColumn(){ return false; }
	get updatedAtColumn(){ return false; }

	customers(){
		return this.hasMany( 'App/Models/Customer' );
	}

	country(){
		return this.hasMany( 'App/Models/Country', 'id', 'country_id' ); 
	}
}

module.exports = State
