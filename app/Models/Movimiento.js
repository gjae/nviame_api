'use strict'

const Model = use('Model')

class Movimiento extends Model {

	static get table(){ return 'customers_movimientos'; }
	static get hidden(){
		return ['customer_id', 'shipment_id','updated_at'];
	}

	customer(){
		return this.belongsTo( 'App/Models/Customer' ,'customer_id' , 'id'); 
	}

	retiro(){
		return this.hasOne('App/Models/Retiro');
	}

}

module.exports = Movimiento
