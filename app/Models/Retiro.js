'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Retiro extends Model {

    static get table(){
        return "retiros";
    }

}

module.exports = Retiro
