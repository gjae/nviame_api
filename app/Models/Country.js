'use strict'

const Model = use('Model')

class Country extends Model {
	static get table() { return 'countries'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	customers(){
		return this.hasMany( 'App/Models/Customer', 'id', 'country_id' );
	}

	states(){
		return this.belongsTo( 'App/Models/Country', 'country_id', 'id' );
	}
}

module.exports = Country
