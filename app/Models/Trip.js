'use strict'

const Model = use('Model')

class Trip extends Model {
	static get table() { return 'trips'; }
	static get createdAtColumn(){ return false; }
	static get updatedAtColumn(){ return false; }

	scales(){
		return this.hasMany('App/Models/Scale', 'id_trip', 'trip_id');
	}

	transport(){
		return this.belongsTo('App/Models/Transport', 'transport_type', 'id');
	}
}

module.exports = Trip
