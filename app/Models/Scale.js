'use strict'

const Model = use('Model')

class Scale extends Model {
	static get table(){ return 'scales'; }
	static get hidden(){ return ['created_at', 'updated_at', 'trip_id']; }

}

module.exports = Scale
