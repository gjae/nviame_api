'use strict'

const Model = use('Model')

class Socialsig extends Model {

	static get table(){ return 'customer_social_login'; }


	customer(){
		return this.belongsTo( 'App/Models/Customer', 'customer_id', 'id' );
	}

}

module.exports = Socialsig
