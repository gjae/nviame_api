'use strict'

const Model = use('Model')

class Brand extends Model {

	static get table(){ return 'brands'; }
	static get hidden(){ return ['created_at', 'deleted_at', 'updated_at']; }

	vehicles(){
		return this.hasMany('App/Models/Vehicle');
	}
}

module.exports = Brand
