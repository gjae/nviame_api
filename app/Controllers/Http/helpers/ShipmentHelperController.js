'use strict'
const Customer = use('App/Models/Customer');
const Shipment = use('App/Models/Shipment');
const Offer = use( 'App/Models/Offer' );
const DB = use('Database');

/**
 * ATRIBUTO: type_shipment SI SE ENCUENTRA COMO VARIABLE DE TIPO GET, ENTONCES
 * SE APLICA EL FILTRO PARA UNICAMNTE OBTENER LOS ENVIOS DE ESE TIPO,
 * SU VALOR ES ID DE UN TIPO DE  ENVIO
 */

const ShipmentHelperController = {
	getAll: async (auth, response, request)=>{
      var user = await auth.getUser();
      var shipments = null;

      var req = request.all();

      if( req.type_shipment == undefined ){
        shipments = await Shipment
        .query()  
        .where('shipments.state', '=', 2)
        .where('shipments.customers_id', '<>', user.id)
        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.customers_id as customer_id',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'customers.id as customers_id',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).orderBy('shipments.id_shipment', 'DESC').distinct();
      }else{
        shipments = await Shipment
        .query()  
        .where('shipments.state', '=', 2)
        .where('shipments.customers_id', '<>', user.id)
        .where('shipments.type_shipment_id', '=', req.type_shipment)
        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.customers_id as customer_id',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'customers.id as customers_id',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).orderBy('shipments.id_shipment', 'DESC').distinct();
      }

      var rating = 0;
      for( var i = 0; i < shipments.length ; i++ ){
        var stars = await DB.from('ratings').where('to_customers_id', '=', shipments[i].customer_id).sum('stars as stars');
        var posts = await DB.from('ratings').where('to_customers_id', '=', shipments[i].customer_id).count('id as posts');

        var rating =  stars[0].stars / posts[0].posts;
        shipments[i].customer_rating = rating ? rating : 0;
        shipments[i].customer_fullname = shipments[i].customer_name+' '+shipments[i].customer_lastname;
        shipments[i].deletable = shipments[i].editable = shipments[i].customers_id == user.id;
      }

      /**
       * ESTE CICLO EXTRAE TODOS LOS ID DE LOS SHIPMENTES QUE LLEGARON DEL RESULTADO DE LA CONSULTA
       * CUANDO SE TERMINA EL CICLO, HACE UNA CONSULTA A LA TABLA OFFERS DONDE SU CAMPO customers_id
       * SEA IGUAL AL ID DEL USUARIO CONECTADO Y SUS ATRIBUTOS shipmentes_id ESTEN PRESENTES EN EL ARREGLO
       * DE LOS ID DEL CICLO ANTES MENCIONADO
       */
      var ship_ids = [];
      for( var i = 0; i < shipments.length; i++){
        ship_ids.push(shipments[i].id_shipment);
      }

      var offers = await Offer.query().where('customers_id', '=', user.id).whereIn('shipments_id', ship_ids);

      /**
       * EL CICLO EXTERNO SE EJECUTA CON LA LONJITUD DEL RESULTADO DE LA CONSULTA A OFFERS
       * EL CICLO INTERNO SE EJECUTA CON LA LONGITUD DE LOS RESULTADOS DE LA CONSULTA A SHIPMENTS
       * EL ARREGLO shipments (EL CUAL ES EL DEVUELTO COMO RESULTADO DE LA PETICION HTTP),
       * SE LE CREA UN ATRIBUTO ADICIONAL (offerted = el usuario conectado ya ha ofertado aqui)
       */
       if( offers.length == 0 ){
//	return {offers, void: offers.length, shipments: shipments.length }

        for(var i =0 ; i < shipments.length; i++){
          shipments[i].offerted = false;
        }
	
       }else{
        for(var i = 0; i < offers.length; i++  ){
          for(var j = 0; j < shipments.length; j++){
            // ESTA COMPARACION ES VERDADERA, UNICAMENTE SI EL ATRIBUTO shipments_id (tabla offers) == id_shipment
            // CUANDO LA COMPARACION ES VERDADERA , ES PORQUE EL USUARIO CONECTADO YA HA REALIZADO UNA OFERTA
            // EN EL shipment CORRESPONDIENTE A LA POSICION "j" DEL ARREGLO DEVUELTO EN LA CONSULTA A SHIPMENTS
            shipments[j].offerted = shipments[j].id_shipment == offers[i].shipments_id;
          } // FIN DEL FOR INTERNO
        } // FIN DEL FOR EXTERNO
      }

      return response.status(202).header('Content-Type', 'application/json').send({error: false, shipments});
	},
	getPending: async (auth, response, request)=>{
      var user = await auth.getUser();
      var shipments = null;
      var req = request.all();
      if( req.type_shipment == undefined ){
        shipments = await Shipment
        .query()  
        .where('customers_id', '=', user.id)
        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).distinct();


        for(var i= 0 ; i < shipments.length; i++ ){
          offers = await Offer.query().where('shipments_id', '=', shipments[i].id_shipment);

          shipments[i].offers = offers;
          shipments[i].total_offers = offers.length;
        } 
      }else{
        shipments = await Shipment
        .query()  
        .where('customers_id', '=', user.id)
        .where('shipments.type_shipment_id', '=', req.type_shipment)

        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).distinct();

        for(var i= 0 ; i < shipments.length; i++ ){
          offers = await Offer.query().where('shipments_id', '=', shipments[i].id_shipment);
          
          shipments[i].offers = offers;
          shipments[i].total_offers = offers.length;
          shipments[i].customer_rating = ( shipments[i].customer_rating == null ) ? 0 : shipments[i].customer_rating;
        } 
      }

      /**
       * ESTE CICLO EXTRAE TODOS LOS ID DE LOS SHIPMENTES QUE LLEGARON DEL RESULTADO DE LA CONSULTA
       * CUANDO SE TERMINA EL CICLO, HACE UNA CONSULTA A LA TABLA OFFERS DONDE SU CAMPO customers_id
       * SEA IGUAL AL ID DEL USUARIO CONECTADO Y SUS ATRIBUTOS shipmentes_id ESTEN PRESENTES EN EL ARREGLO
       * DE LOS ID DEL CICLO ANTES MENCIONADO
       */
      var ship_ids = [];
      for( var i = 0; i < shipments.length; i++){
        ship_ids.push(shipments[i].id_shipment);
      }

      var offers = await Offer.query().where('customers_id', '=', user.id).whereIn('shipments_id', ship_ids);

      /**
       * EL CICLO EXTERNO SE EJECUTA CON LA LONJITUD DEL RESULTADO DE LA CONSULTA A OFFERS
       * EL CICLO INTERNO SE EJECUTA CON LA LONGITUD DE LOS RESULTADOS DE LA CONSULTA A SHIPMENTS
       * EL ARREGLO shipments (EL CUAL ES EL DEVUELTO COMO RESULTADO DE LA PETICION HTTP),
       * SE LE CREA UN ATRIBUTO ADICIONAL (offerted = el usuario conectado ya ha ofertado aqui)
       */

       if( offers.length == 0 ){
        for(var i =0 ; i < shipments.length; i++){
          shipments[i].offerted = false;
        }
       }else{
        for(var i = 0; i < offers.length; i++  ){
          for(var j = 0; j < shipments.length; j++){
            // ESTA COMPARACION ES VERDADERA, UNICAMENTE SI EL ATRIBUTO shipments_id (tabla offers) == id_shipment
            // CUANDO LA COMPARACION ES VERDADERA , ES PORQUE EL USUARIO CONECTADO YA HA REALIZADO UNA OFERTA
            // EN EL shipment CORRESPONDIENTE A LA POSICION "j" DEL ARREGLO DEVUELTO EN LA CONSULTA A SHIPMENTS
            shipments[j].offerted = shipments[j].id_shipment == offers[i].shipments_id;
          } // FIN DEL FOR INTERNO
        } // FIN DEL FOR EXTERNO
      }

      return response.status(202).header('Content-Type', 'application/json').send({error: false, shipments});
	},
	getConcreted: async (auth, response, request)=>{
      var user = await auth.getUser();
      var shipments = null;
      var req = request.all();

      if( req.type_shipment == undefined){
        shipments = await Shipment
        .query()  
        .where('shipments.state', '=', 8)
        .where('customers_id', '=', user.id)
        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).distinct();
      }else{
        shipments = await Shipment
        .query()  
        .where('shipments.state', '=', 8)
        .where('customers_id', '=', user.id)
        .where('shipments.type_shipment_id', '=', req.type_shipment)
        .innerJoin('customers', 'customers.id', '=', 'shipments.customers_id')
        .innerJoin('categories', 'shipments.category_id', '=', 'categories.id')
        .innerJoin('type_shipments', 'type_shipments.id', '=', 'shipments.type_shipment_id')
        .select(
            'shipments.id_shipment', 
            'shipments.address_from', 
            'shipments.address_to',
            'shipments.package_description',
            'shipments.urgency',
            'shipments.price',
            'shipments.picture',
            'shipments.width',
            'shipments.height',
            'shipments.width',
            'shipments.weight',
            'shipments.depth',
            'shipments.urgency',
            'categories.name as category_name',
            'categories.id as category_id',
            'customers.name as customer_name',
            'customers.lastname as customer_lastname',
            'customers.image as customer_photo',
            'customers.rating as customer_rating',
            'type_shipments.id as type_id',
            'type_shipments.type_name'
        ).distinct();
      }

      var ratings = 0;
      for( var i = 0; i < shipments.length ; i++ ){
        var stars = await DB.from('ratings').where('to_customers_id', '=', shipments[i].customer_id).sum('stars as stars');
        var posts = await DB.from('ratings').where('to_customers_id', '=', shipments[i].customer_id).count('id as posts');

        ratings =  stars[0].stars / posts[0].posts;
        shipments[i].customer_rating = ratings ? ratings : 0;
        shipments[i].customer_fullname = shipments[i].customer_name+' '+shipments[i].customer_lastname;
      }
      /**
       * ESTE CICLO EXTRAE TODOS LOS ID DE LOS SHIPMENTES QUE LLEGARON DEL RESULTADO DE LA CONSULTA
       * CUANDO SE TERMINA EL CICLO, HACE UNA CONSULTA A LA TABLA OFFERS DONDE SU CAMPO customers_id
       * SEA IGUAL AL ID DEL USUARIO CONECTADO Y SUS ATRIBUTOS shipmentes_id ESTEN PRESENTES EN EL ARREGLO
       * DE LOS ID DEL CICLO ANTES MENCIONADO
       */
      var ship_ids = [];
      for( var i = 0; i < shipments.length; i++){
        ship_ids.push(shipments[i].id_shipment);
        shipments[i].customer_rating = ( shipments[i].customer_rating == null ) ? 0 : shipments[i].customer_rating;
      }

      var offers = await Offer.query().where('customers_id', '=', user.id).whereIn('shipments_id', ship_ids);

      /**
       * EL CICLO EXTERNO SE EJECUTA CON LA LONJITUD DEL RESULTADO DE LA CONSULTA A OFFERS
       * EL CICLO INTERNO SE EJECUTA CON LA LONGITUD DE LOS RESULTADOS DE LA CONSULTA A SHIPMENTS
       * EL ARREGLO shipments (EL CUAL ES EL DEVUELTO COMO RESULTADO DE LA PETICION HTTP),
       * SE LE CREA UN ATRIBUTO ADICIONAL (offerted = el usuario conectado ya ha ofertado aqui)
       */
      for(var i = 0; i < offers.length; i++  ){
        for(var j = 0; j < shipments.length; j++){
          // ESTA COMPARACION ES VERDADERA, UNICAMENTE SI EL ATRIBUTO shipments_id (tabla offers) == id_shipment
          // CUANDO LA COMPARACION ES VERDADERA , ES PORQUE EL USUARIO CONECTADO YA HA REALIZADO UNA OFERTA
          // EN EL shipment CORRESPONDIENTE A LA POSICION "j" DEL ARREGLO DEVUELTO EN LA CONSULTA A SHIPMENTS
          shipments[j].offerted = shipments[j].id_shipment == offers[i].shipments_id;
        } // FIN DEL FOR INTERNO
      } // FIN DEL FOR EXTERNO

      return response.status(202).header('Content-Type', 'application/json').send({error: false, shipments});
	},


}

module.exports = ShipmentHelperController
