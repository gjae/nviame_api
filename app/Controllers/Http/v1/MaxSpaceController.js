'use strict'

const MaxSpace = use('App/Models/MaxSpace');

class MaxSpaceController {
  async index ({ response }) {

    const max_spaces = await MaxSpace.all();

    return response.status(202).send({ error: false, max_spaces });
  }

  async create () {
  }

  async store () {
  }

  async show () {
  }

  async edit () {
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = MaxSpaceController
