'use strict'
const Customer = use('App/Models/Customer');
const Shipment = use('App/Models/Shipment');
const Offer = use( 'App/Models/Offer' );
const DB = use('Database');
const ShipsHelpers = use('App/Controllers/Http/helpers/ShipmentHelperController');
const { validate } = use('Validator');
const Category = use('App/Models/Category');
const Type = use('App/Models/Typeshipment');
const moment = require('moment');
var base64Img = require('base64-img');
const Helpers = use ('Helpers');

const crypto = require('crypto');

/**
 * CONTROLADOR PARA LA GESTION DE LOS ENVIOS, RETORNA TODO LO REFERENTE A LA TABLA SHIPMENTS,
 * DESDE LA LISTA DE ENVIOS DISPONIBLES HASTA CREAR NUEVOS
 * POSIBLES CODIGOS DE ERROR: 
 * |__ 401: UNAUTHORIZED (EN CASO DE QUE EL LOGIN O LA AUTORIZACION DEL TOKEN NO SEA CORRECTA)
 * |__ 202, 201, 200: OK, LA PETICION HA SIDO ACEPTADA
 * |__ 500: ERROR INTERNO DEL SERVIDOR
 *
 * _______________________________________________________________________________________
 * |
 * |                    CODIGOS DISPONIBLES PARA LOS STATES DE LOS SHIPMENTS
 * |______________________________________________________________________________________
 * |
 * | 0: DISPONIBLE SIN ASIGNAR
 * | 1: ASIGNADO NO CONCRETADO
 * | 2: CONCRETADOS (ASIGNADOS, COMPLETADO Y PAGADO)
 * |______________________________________________________________________________________
 * @author Giovanny Avila gjavila1995@gmail.com
 * @date 14 Apr, 2018
 */

class ShipmentController {

  _capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  /**
   * RETORNA UNA LISTA DE LOS shipments CUYO STATE = 0 (RECIBIENDO OFERTAS)
   * @param   options.response OBJETO PARA EJECUTAR RESPUESTA HTTP
   * @param  DATOS DE AUTH options.auth      DATOS DE LA PERSONA CONECTADA
   * @return JSON                  HTTP RESPONSE
   */
  async index ({ response , auth, request }) {
    try{

      var input = ( request.all().type !== undefined ) ? request.input('type') : 'all';
      return ShipsHelpers[ 'get'+this._capitalize( input ) ](auth,response,request);

    }catch(error){
      return response.status(500).send({error});
    }
  }

  async create () {
  }

  async _savePicture( dataRequest  ){

    if( dataRequest.picture ) {

      if( dataRequest.picture.indexOf("base64") == -1 ) {
        console.log("PICTURE EMPTY");
        delete dataRequest.picture;
        return dataRequest;
      }

      console.log( "TIENE PICTURE" );
      let filename = crypto.createHash('md5').update( moment( new Date() ).format('YYYY-MM-DD HH:MM:SS') ).digest('hex');
      console.log( filename );

      let path = base64Img.imgSync(dataRequest.picture.trim() , Helpers.publicPath('img/shipments') , filename);

      filename = path.split('/');

      dataRequest.picture = filename[ ( filename.length - 1)  ];


      return dataRequest;
      
    }

    return dataRequest;

  }

  async store ({ auth, request, response, params }) { 

    try{


      let ultimo_id = await DB.from('shipments').max('id_shipment as id_shipment').first();
      ultimo_id.id_shipment ++;
      let dataRequest = request.all();


      dataRequest = await this._savePicture( dataRequest );

      let session = await auth.getUser();

      let user = { customers_id: session.id, state: 2, pin: '--'};

      let { identification } = await auth.getUser();

      if( identification == null )
        identification = '1234567891';

      /**
       * EL PIN SE CREA A PARTIR DEL NUEVO ID QUE SE CREARA
       * COMPLETANDO CON 0 A LA IZQUIERDA SI ESE ID ES MENOR A 4
       * CIFRAS, Y CONCATENANDOSELE 3 DIGITOS ALEATORIOS DEL NUMERO
       * DE IDENTIFICACION DEL USUARIO QUE ESTA PUBLICANDO EL NUEVO
       * ENVIO
       * @type string
       * @var { pin } CODIGO DEL PIN
       */
      let pin = ultimo_id.id_shipment.toString();
      identification =  typeof(identificacion) ? identification.toString() : identification;
      for( var i = 0; i <3 ; i++){
        pin += identification.charAt( 
          Math.floor( Math.random() * identification.length ) 
        );
      }


      dataRequest = Object.assign(dataRequest, user, ultimo_id);
      dataRequest.max_arrive_date = ( dataRequest.max_arrive_date == "" ) ? null : dataRequest.max_arrive_date;


      var shipment = await Shipment.create(dataRequest);

      for( var i = ultimo_id.id_shipment.toString().length; i < 4; i++ ){
        pin = '0'+pin;
      }

      /**
       * POR ULTIMO SE ACTUALIZA EL NUEVO REGISTRO EN LA TABLA
       * PARA ASIGNARSE SU CODIGO DE PIN
       * @type { promise }
       */
      await DB.from('shipments').where('id_shipment', '=', ultimo_id.id_shipment).update({pin: pin});

      shipment.pin = pin;
      return response.status(202).header('Content-Type', 'application/json').send({error: false, shipment});

    }catch(error){
      return response.status(500).header('Content-Type', 'application/json').send({error});
    }

  }

  async show ({ auth, request, response, params }) {
    try{

      var user =  await auth.getUser();

      var shipment = await Shipment.query().where('id_shipment', '=', params.id).with('customer').first();
      var offer = await Offer
      .query()
      .where('shipments_id', '=', params.id)
      .where('customers_id', '=', user.id)
      .whereNull('offers.deleted_at')
      .count('* as offers').first();

      var offers = await Offer.query().where('shipments_id', '=', shipment.id_shipment);

      return response.status(202).header('Content-Type', 'application/json').send({ error: false, offers ,shipment , offerted: ( offer.offers > 0 ) ? true : false });
     
    }catch(error){
      return response.header('Content-Type', 'application/json').status(504).send({ error });
    }
  }

  async edit () {
  }

  async update ({ auth, request, response, params }) {
    var dataRequest = request.all();
    var user = await auth.getUser();
    var offer = await Shipment
    .query()
    .where('id_shipment', '=', params.id)
    .where('customers_id', '=', user.id)
    .whereNull('deleted_at')
    .first();

    if( typeof dataRequest.pin != "undefined" )
      delete dataRequest.pin;

    let getOffersOfShipment = await Offer.query().where('id_shipment', '=', offer.shipments_id);

    if( getOffersOfShipment ) 
      return response.status(202).send({error : true, message: "Has offer"});

    for( var object in dataRequest ){

      if( object == "picture" ){
        dataRequest = await this._savePicture( dataRequest );
      }
      offer[object] = dataRequest[object];
    }

    await offer.save();
    return response.status(202).send({error: false, shipment: offer});
  }

  async destroy ({ auth, params, response}) {

    var offers = await Offer
    .query()
    .where('shipments_id', '=', params.id);
    if( offers.state == 2){
      await Shipment
      .query()
      .where('id_shipment', '=', params.id)
      .update({ deleted_at: moment( new Date() ).format('YYYY-MM-DD HH:mm:ss') });
      return response.status(292).send({ error: false, message: "Operation success" });
    }

    return response.status(202).send({ error: true, message: "Shipment in course" });

  }

  async categories({ response }){
    var categories = await Category.all();

    return response.status(202).send({ error: false, categories});
  }

  async types({ response }){
    var types = await Type.query().whereNull('deleted_at').select('id', 'type_name', 'type_description', 'icon', 'separators', 'css_style');

    return response.status(202).send({ error: false, types });
  }


  async entrega({auth, request, response}){

  }
}

module.exports = ShipmentController
