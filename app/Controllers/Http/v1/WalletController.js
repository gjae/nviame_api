'use strict'
const Shipment = use('App/Models/Shipment');
const moment = require('moment');
const { validate } = use('Validator');
const Movimiento = use( 'App/Models/Movimiento' );
const Categoria = use('App/Models/Category');
const Retiro = use('App/Models/Retiro');
const Offer = use('App/Models/Offer');


class WalletController {
  async index ({ auth, request, response, params }) {
    const user = await auth.getUser();

    let movimientos = await this._getAll(auth, user);


    let anio = "";
    let creds = {};
    let data = {};

    let total_credito = 0, total_debito = 0;

    for(var i = 0; i< movimientos.length; i++){
      let movimiento = movimientos[i];
      let { created_at } = movimiento;

      let index =  moment( created_at ).format("Y-MM").split("-");

      if( anio != index[0] ){
        anio = index[0];
        creds = {};
        data[index[0]] = ( data[index[0]] ) ? data[index[0]] : {};
      }


      creds[ index[1] ] = ( creds[ index[1] ] ) ? creds[index[1] ] : { ingreso: 0, egreso: 0 };
      if( movimiento.tipo == "C"  ){
        creds[ index[1] ].ingreso += parseFloat(movimiento.monto);
        total_credito += parseFloat(movimiento.monto);
      }
      else if( movimiento.tipo == "D" ){
        creds[ index[1] ].egreso += parseFloat(movimiento.monto);
        total_debito += parseFloat(movimiento.monto);
      }

      data[ index[0] ] = creds;
    }

    total_debito == null ? 0.00 : total_debito;
    var respuesta = {
      data,
      total_ingreso: total_credito,
      total_egreso: total_debito,
      total: total_credito - total_debito
    };

    return response.status(202).send({error: false, ...respuesta});
  } 

  /**
   * METODO PARA OBTENER LOS DATOS DE LOS CREDITOS CORRESPONDIENTES A LOS MOVIMIENTOS 
   * CONTABLES DEL USUARIO CONECTADO
   * @return CREDITOS      [description]
   */
  async _getCreditos(auth, user){

    // BUSQUEDA DE MOVIMIENTOS DE TIPO CREDITO EN LA BD
    var creditos_db = await Movimiento
    .query()
    .where('customer_id', '=', user.id)
    .where('tipo', '=', 'C')
    .orderBy('created_at');

    var total = {};
    var credito = 0.00;
    // CREDITOS CORRESPONDIENTES A LAS PRE-CARGAS DE LA CUENTA DEL USUARIO
    for( var mov in creditos_db ){
      var index =  moment( creditos_db[mov].created_at ).format('YYYY-MM');
      total[ index ] = ( total[index] ) ? total[index] : 0;
      total[ index ] += parseFloat( creditos_db[ mov ].monto );
      credito += parseFloat( creditos_db[ mov ].monto );
    }

    return { total_credito: credito ,  creditos: total };
  }

  async _getAll(auth, user){
    var  movimientos = await Movimiento
    .query()
    .where('customer_id', '=', user.id)
    .orderBy('created_at', 'DESC');

    return movimientos;
  }

  /**
   * ESTE METODO BUSCA EN LA BASE DE DATOS LOS CREDITOS ASIGNADOS AL USUARIO CONECTADO
   * MEDIANTE ENVIOS OFERTAS ACEPTADAS
   */
  async _creditosPorEnvios(auth, user, tipo = 'offers.customers_id'){
    // BUSQUEDA DE LOS PAGOS MEDIANTES ENVIOS CON ESTATUS COMPLETADOS (8)
    var envios = await Shipment
    .query()
    .where( query => query.where('state', '=', 8).orWhere('state', '=', 13) )
    .innerJoin( 'offers', 'offers.shipments_id', '=', 'shipments.id_shipment' )
    .where( tipo , '=', user.id)
    .whereNotNull( 'accepted_date' )
    .select('shipments.*')
    .orderBy('shipment_at', 'DESC')
    .with( 'offers', query => query.where( tipo , '=', user.id).whereNotNull( 'accepted_date' ) ).fetch();

    envios = envios.toJSON();
    var { total, credito } = await this._getCreditos(auth, user);

   // CREDITOS CORRESPONDIENTES A LOS ENVIOS REALIZADOS CORRECTAMENTE
    for( var envio in envios ){

      for( var offer in envios[envio].offers ){
        var index =  moment(envios[envio].offers[offer].date_added ).format('YYYY-MM');
        total[ index ] = ( total[index] ) ? total[index] : 0;
        credito = total[ index ] += parseFloat( envios[envio].offers[offer].price_offer);
      }

    }

    return { total, envios, credito };

  }

  /**
   * OBTIENE LOS DEBITOS GENERADOS POR UN USUARIO ATRIBUIDOS A GASTOS POR PAGOS DE ENVIOS
   * REUTILIZANDO LA FUNCION DE CREDITOS POR ENVIOS, PASANDO EL TERCER ATRIBUTO CON EL VALOR 'to_customers_id' 
   * QUE HACE REFERENCIA AL USUARIO QUE SE LE COBRA EL ENVIO
   * @param  OBJECT auth DATOS DE LA SESSION DEL USUARIO ACTIVO
   * @param  OBJECT user USUARIO CONECTADO
   * @return OBJECT      RETORNA UB OBJETO COMPUESTO POR LOS MISMOS DATOS DE LA FUNCION DE CREDITOS POR EL ENVIO
   *                             USANDO EL CREDITO COMO DEBITO
   */
  async _debitosPorEnvios(auth, user){
    var { total, envios, credito } = await this._creditosPorEnvios(auth, user, 'to_customers_id');

    return { total, envios, debito: credito };
  }

  async _getDebitos(auth, user){

    // BUSCANDO LOS DEBITOS EN LA TABLA DE MOVIMIENTOS DEL USUARIO
    var debitos_db = await Movimiento
    .query()
    .where('customer_id', '=', user.id)
    .where('tipo', '=', 'D')
    .orderBy('created_at', 'DESC');

    var debitos = {};
    var debito = 0.00;

    // DEBITOS CORRESPONDIENTES A LOS MOVIMIENTOS NEGATIVOS DEL USUARIO
    for( var mov in debitos_db ){
      var index =  moment( debitos_db[mov].created_at ).format('YYYY-MM');
      debitos[ index ] = ( debitos[index] ) ? debitos[index] : 0;
      debitos[ index ] += parseFloat( debitos_db[ mov ].monto );
      debito += parseFloat( debitos_db[ mov ].monto );
    }

    return { debitos, total_debito: debito };

  }

  async create () {
  }

  async _getSum(user, tipo = 'D'){
    let debitos = await Movimiento.query().where('customer_id', '=', user.id).where('tipo', '=', tipo).sum('monto as total');

    return parseFloat(debitos[0].total == null ? 0.00 :debitos[0].total);
  }

  async store ({ auth, request, response }) {



    const user = await auth.getUser();
    var dataRequest = request.all();


    var fails = await this._validate(dataRequest);
    if( dataRequest.tipo == 'C' &&  fails ){

      dataRequest = Object.assign(dataRequest, { 
        customer_id: user.id
      });

      var movimiento = await Movimiento.create( dataRequest );
     // movimiento =  await this._getMovimiento(movimiento.id);

      return response.status(202).send({ error: false, movimiento });
    }else if( dataRequest.tipo == 'D' ){

      // SE OBTIENE EL TOTAL DE CREDTOS Y DEBITOS PARA VERIFICAR LOS FONDOS DISPONIBLES
      var total_credito  = await this._getSum(user, 'C');
      var total_debito = await this._getSum(user, 'D');

      
      // VALIDACION DE SALDO SUFICIENTE PARA REALIZAR EL DEBITO SOLICITADO
      if( total_credito - (total_debito + parseFloat(dataRequest.monto)) > 0 ){
        dataRequest = Object.assign(dataRequest, { customer_id: user.id });
        var movimiento = await Movimiento.create( dataRequest );

        if( typeof dataRequest.shipment_id != "undefined" && dataRequest.shipment_id > 0 ){
          let offer = await Offer.query().where('shipments_id', '=', dataRequest.shipment_id).whereNotNull('accepted_date').first();
          let dataMov = {
            tipo: "DIF",
            origen: "MEP",
            monto: offer.price_offer,
            nro_referencia: "00000PAY",
            customer_id: offer.customers_id,
            shipment_id: dataRequest.id_shipment
          }
          let mov2 = await Movimiento.create(dataMov);
        }

        return response.status(202).send({ error: false, movimiento });
      }

      return response.status(202).send({ error: true, message: 'Fondo insuficiente', disponible: total_credito - total_debito, solicitado: parseFloat(dataRequest.monto) });
    }


    return response.status(202).send({ error: true, message: 'Invalid arguments' });
  }

  

  async _validate(data){
    var validation = await validate( data , {
      nro_referencia: 'required',
      monto: 'required', 
      origen: 'required',
      shipment_id: 'required'
    });


    return  !validation.fails();
  }

  async _getMovimiento(id){
    var movimiento = await Movimiento
      .query()
      .where('id', '=', id )
      .with('customer').first();
  }

  async show ({ params, request, response, auth }) {
    let firstOftMonth = moment( params.id.split("-") ).subtract(1, "months") ;
    let endOfMonth = ( request.all().hasta == undefined ) ? moment( params.id.split("-") ) : moment( request.all().hasta.split("-") );
    let credito = 0;
    let debito = 0;
    
    let movimientos = await Movimiento
    .query()
    .where('created_at', '>=', firstOftMonth.format('Y-MM-DD') )
    .where('created_at', '<=', endOfMonth.subtract("days", 1).format('Y-MM-DD') )
    .where("customers_movimientos.tipo", '<>', 'DIF')
    .leftJoin('shipments', 'customers_movimientos.shipment_id', '=', 'shipments.id_shipment')
    .orderBy('created_at', 'DESC');

    let movimientosResponse = [];
    for(var i = 0; i < movimientos.length; i ++ ){

      let categoria = await Categoria.find( movimientos[i].category_id );

      movimientosResponse[i] = {
        debito: ( movimientos[i].tipo == "D" ) ? parseFloat(movimientos[i].monto) : 0,
        credito: ( movimientos[i].tipo == "C" ) ? parseFloat(movimientos[i].monto) : 0,
        fecha: moment( movimientos[i].created_at ).format('Y-MM-DD')
      };

      // SI EL MOVIMIENTO TIENE UN ID DE ENVIO (shipment_id) DIFERENTE A 0, QUIERE DECIR QUE FUE
      // UN MOVIMIENTO GENERADO POR UN ENVIO , DE SER ASO, ENTONCES SE AGREGA LOS DETALLES DEL ENVIO EN EL
      // OBJETO ENVIO, DE CASO CONTRARIO, EL OBJETO ENVIO SE RETORNA EN "false" PARA INDICAR
      // QUE EL MOVIMIENTO NO FUE GENERADO POR UN ENVIO (PUDO HABER SIDO GENERADO, POR EJEMPLO, POR UN RETIRO DE FONDOS)
      if( movimientos[i].shipment_id != 0 ){
        movimientosResponse[i] = Object.assign({}, movimientosResponse[i], {
          envio: {
            desde: movimientos[i].address_from,
            hasta: movimientos[i].address_to,
            categoria: categoria.name,
            descripcion: movimientos[i].package_description
          }
        });
      }else {
        movimientosResponse[i] = Object.assign({}, movimientosResponse[i], { envio: false });
      }

      credito += ( movimientos[i].tipo == "C" ) ? parseFloat(movimientos[i].monto) : 0;
      debito += ( movimientos[i].tipo == "D" ) ? parseFloat(movimientos[i].monto) : 0;
    }

    return response.status(202).send( {
      movimientos: movimientosResponse,
      principio: firstOftMonth.format('Y-MM-DD'), 
      final:  ( request.all().hasta == undefined ) ? moment( params.id.split("-") ).subtract(1, "days").format("Y-MM-DD") : moment( request.all().hasta.split("-") ).subtract(1, "days").format('Y-MM-DD'), 
      debito, 
      credito ,
      disponible: credito - debito,
      hasta: request.all().hasta == undefined
    });

  }

  async retiro ({ request, response, auth }) {
    var validation = await validate(request.all(), {
      monto: 'required'
    });

    if( !validation.fails() ){
      const user = await auth.getUser();
      let dataRequest = request.all();
      dataRequest = Object.assign({}, dataRequest, {
        customer_id: user.id,
        tipo: 'DIF',
        nro_referencia: '00000RETIRO',
        shipment_id: 0,
      });

      let movimiento = await Movimiento.create(dataRequest);

      if(movimiento)
        return response.status(202).send({error: false, movimiento});
    }

    return response.status(202).send({ error: true, message: "Fail operation" });
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = WalletController
