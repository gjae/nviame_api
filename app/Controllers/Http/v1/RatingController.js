'use strict'

const Rating = use('App/Models/Rating');
const Customer = use('App/Models/Customer');
const Shipment = use('App/Models/Shipment');
const DB = use('Database');
const moment = require('moment');
const Offer = use('App/Models/Offer');

class RatingController {
  async index ({ auth, response, request }) {
    const user = await auth.getUser();
    return user;
  }

  async create () {
  }

  async store ({ auth, params, request, response }) {
    const ship = await Shipment.query()
    .where('id_shipment', '=', request.all().shipment)
    .where('shipments.state', '=', 8)
    .innerJoin('offers', 'shipments.id_shipment', '=', 'offers.shipments_id')
    .whereNotNull('accepted_date')
    .first();

    if(ship){
      const user = await auth.getUser();

      var offer = await Offer.query().where('id', '=', ship.id).first();


      var insertData = Object.assign(request.only(['stars', 'comments']), {
        date_added: moment(new Date() ).format('YYYY-MM-DD hh:mm:ss'),
        from_customers_id: user.id,
        to_customers_id: offer.customers_id
      });

      var rating = await Rating.create( insertData );
      if( rating ){
        var shipment = await DB.from('shipments').where('id_shipment', '=', request.all().shipment).update({ state: 14 });
        
        return response.status(202).send({ error: false, rating });
      }
    }
    return response.status(404).send({ error: false, message: 'Resource not found'});
  }

  async show ({  auth, response, request , params}) {
    const ratings = await Rating.query().where('to_customers_id', '=', params.id).with('from').fetch();
    const stars = await Rating.query().where('to_customers_id', '=', params.id).sum('stars as stars');
    const countPosts = await Rating.query().where('to_customers_id', '=', params.id).count('id as posts');
    
    var posts = countPosts[0].posts;
    var total_stars = ( posts > 0 &&  stars[0].stars != null ) ? stars[0].stars / countPosts[0].posts : 0;


    var response_data = {
      ratings,
      total_stars,
      total_shipments_califications: countPosts[0].posts
    }

    return response.status(202).send(response_data);
  }

  async edit () {
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = RatingController
