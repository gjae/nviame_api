'use strict'


const Offer = use('App/Models/Offer');
const moment = require('moment');
const DB = use('Database');
const Shipment = use('App/Models/Shipment');
const Delivery = use('App/Models/Delivery');
const { validate } = use('Validator');



class DeliveryController {
  async index () {
  }

  async create () {
  }

  /**
  * CUANDO LA OFERTA ES ACEPTADA, SE ENVA UNA PETICION POST,
  * SE CREA EL REGISTRO EN DELIVERIES COMO LISTO PARA ENTREGAR
  */

  async store ({ auth, request, response, params }) {

    var dataRequest = request.all();

    var validation = await validate(dataRequest,{
      offer_id: 'required'
    });

    if( validation.fails() ){ return response.status(404).send({ error: true, message: 'Invalid request'}); }

    /**
     * SE CONSULTA LA BD SI YA EXISTE UN DELIVERY PARA LA OFERTA
     */

    var offer = await Offer
    .query()
    .whereNull('offers.deleted_at')
    .where('offers.id', '=', dataRequest.offer_id)
    .innerJoin('shipments', 'shipments.id_shipment', '=', 'offers.shipments_id')
    .leftJoin('deliveries', 'deliveries.shipments_id', '=', 'shipments.id_shipment')
    .select('offers.*', 'deliveries.id as delivery_id')
    .first();

    // SI NO EXISTE UN DELIVERY CON LA OFERTA ENVIADA COMO PARAMETRO DE LA SOLICITUD
    // (Number.isInteger(offer.delivery_id) == false si la consulta anterior no devuelve un delivery existente para el id de la oferta entrante )
    // SI NO EXISTE UN DELIVERY, SE CREA Y SE RETORNA COMO RESPUESTA A LA PETICION
    // SI YA EXISTE UN DELIVERY PARA LA OFERTA ENTANTE ENTONCES SE DEVUELE EL DELIVERY
    // EXISTENTE
    var date = moment(new Date() ).format('YYYY-MM-DD H:m:s');
    if( !Number.isInteger(offer.delivery_id) ){

      dataRequest =  { 
        shipments_id:  offer.shipments_id,
        customers_id: offer.customers_id,
        status: 4,
        date_start: date,
        date_end: date
      };

      var delivery = await Delivery.create(dataRequest);
      var delivery = await Delivery.query().where('id', '=', delivery.id).with('customer').first();
      offer.merge({ accepted_date: date });

      const shipment = await Shipment.query()
      .where('id_shipment', '=', offer.shipments_id)
      .update({ state: 3 });
      
      offer = await offer.save();
      if( delivery ) return response.status(202).send({ error: false, delivery: delivery, offer });
    }

    var delivery = await Delivery.query().where('id', '=', offer.delivery_id).with('customer').first();

    return response.status(202).send({ error: false, delivery });
  }

  async show ({ params, response }) {
    var delivery = await Delivery
    .query()
    .where('id', '=', params.id)
    .whereNull('deleted_at')
    .with('customer')
    .with('shipment')
    .first();

    if( delivery )
      return response.status(202).send({error: false, delivery});

    return response.status(404).send({ error: true, message: 'Invalid resource'});
  }

  async edit () {
  }

  async update () {
  }

  async destroy ({ auth, request, response, params }) {

    /**
     * UNICAMENTE SE PUEDE ELIMINAR
     * LOS DELIVERIES QUE TENGAN UN STATUS = 4
     * ( ESPERANDO RETIRO )
     * SI UNO ESTA EN NO SE ENCUENTRA RETORNA UN CODIGO 404
     */

    const user = await auth.getUser();
    var delivery = await Delivery
    .query()
    .where('id', '=', params.id)
    .innerJoin('shipments', 'deliveries.shipments_id', '=', 'shipments.id_shipment')
    .where('shipments.customers_id', '=', user.id)
    .where( 'deliveries.status', '=', 4)
    .whereNull('deliveries.deleted_at')
    .select(
      'deliveries.id as delivery_id',
      'shipments.id_shipment as id_shipment'
    )
    .first();

    if( delivery ){
      var date = moment( new Date() ).format('YYYY-MM-DD H:m:s');
      var deleteDelivery = await DB.from('deliveries').where('id', '=', delivery.delivery_id).update({ deleted_at: date });
      var returnShipStatus = await DB.from('shipments').where('id_shipment', '=', delivery.id_shipment).update({ state: 2 });

      return response.status(202).send({ error: false, message: 'Success delete' })
    }

    return response.status(404).send({error: true, message: 'Resource not found'});

  }
}

module.exports = DeliveryController
