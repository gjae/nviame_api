'use strict'

const Rate = use('App/Models/Rate');
const { validate } = use ('Validator');
const Customer = use('App/Models/Customer');
const DB = use('Database');
const Moment = use('moment');

class RatessController {
  async index ({ auth, response, request }) {
    const user = await auth.getUser();

    var rates = await Rate.query().where('customers_id', '=', user.id).with('customer').fetch();
    return response.status(202).send({error: false, rates});
  }

  async create () {
  }

  async store ({ auth, request, response }) {
    
    var dataRequest = request.all();
    const validation = await validate(dataRequest, {
      fixed_price: 'required',
      per_km: 'required',
      surplus_km: 'required'
    });

    if( validation.fails() ) response.status(404).send({ error: true, message: 'Invalid Params' });

    const user = await auth.getUser();
    dataRequest = Object.assign(dataRequest, {customers_id: user.id});
    const rate = await Rate.create(dataRequest);

    if( rate ){
      return response.status(202).send({error: false, rate});
    }

  }

  async show ({ auth, request, response, params }) {
    const user = await auth.getUser();
    var rate = await Rate.query().where('id', '=', user.id).whereNull('deleted_at').with('customer').first();
    if( rate ) return response.status(202).send({ error: false, rate });

    return response.status(404).send({ error: true, message: 'Invalid request'});
  }

  async edit () {
  }

  async update ({ auth, response, request, params }) {
    const user = await auth.getUser();
    const dataRequest = request.all();
    var rate = await Rate.query().where('customers_id', '=', user.id).first();

    if( rate ){

      for( var property in dataRequest ){
        rate[property] = dataRequest[property];
      }

      await rate.save();

      return response.status(202).send({error: false, rate});

    }

    return response.status(404).send({error: true, message: 'Invalid request'});
  }

  async destroy ({ auth, request, response, params }) {
    const user = await auth.getUser();
    var rate = await Rate.query().where('id', '=', params.id).whereNull('deleted_at').first();
    if( rate ){
      const moment = Moment( new  Date() ).format('YYYY-MM-DD H:m:s');
      rate.merge({ deleted_at: moment });
      if( rate.save() ) return response.status(202).send({ error: false, message: 'Success'});
    }
    
    return response.status(404).send({ error: true, message: 'Invalid request' });
  }

}

module.exports = RatessController
