'use strict'

const Vehicle = use('App/Models/Vehicle');
const { validate } = use('Validator');
const moment = require('moment');
const BrandModel = use('App/Models/Bmdl');
const Transport = use('App/Models/Transport');

const crypto = require('crypto');

const Helpers = use('Helpers')

const buffer = require('buffer');
const fs = require('fs');


var base64Img = require('base64-img');
/**
 * CONTROLADOR PARA LOS EL MANEJO DE LOS VEHICULOS
 * POSIBLES VARIABLES GET:
 *   -> from = SI SE DEJA EN BLANCO O SU VALOR ES "auth" ENTONCES SE RETORNARAN LOS VEHICULOS CORRESPONDIENTES
 *     AL USUARIO CONECTADO PARA ESE MOMENTO, SI SU VALOR ES "customer" ENTONCES DEBERA LLEGAR TAMBIEN UN PARAMETRO
 *     "user_id" CON EL ID DEL USUARIO AL QUE SE LE CONSULTARA LOS VEHICULOS
 *   -> user_id = ID DE UN USUARIO ESPECIFICO AL QUE SE LE CONSULTARAN LOS VEHICULOS
 */
class VehicleController {


  async index ({ auth, response, request }) {

    var vehicles = null;
    var dataRequest = request.all();
    if( dataRequest.from == 'auth' || dataRequest.from == undefined ){
      var user = await auth.getUser();
      vehicles = await Vehicle
      .query()
      .where('customers_id', '=', user.id)
      .whereNull('deleted_at')
      .with('transport')
      .with('model')
      .with('brand')
      .fetch();
      dataRequest.from = 'auth';

    }
    else if( dataRequest.from != undefined && dataRequest.from  == 'customer' ){
      if( dataRequest.user_id == undefined ) return response.status(404).send({ error: true, message: 'Malformed URL'});
      else{
        vehicles = await Vehicle.query().whereNull('deleted_at').where('customers_id', '=', request.all().user_id);
      }
    }

    return response.status(202).send( { error: false, vehicles, from: dataRequest.from } );

  }

  async create () {
  }

  async makeImage(b64code){

    if( b64code == undefined  || b64code == "" )
      return null;

    console.log("ENTRO EN LA FUNCION DE IMAGENES");
    let filename = crypto.createHash('md5').update( moment( new Date() ).format('YYYY-MM-DD HH:MM:SS') ).digest('hex');
    console.log(filename);
    let path = base64Img.imgSync(b64code.trim() , Helpers.publicPath('img/vehicles') , filename);
    filename = path.split('/');
    return ( path ) ? filename[ (filename.length - 1) ] : null;
  }


  async store ({ auth, request,response }) {
    
    console.log("STORE")
    try{

      var user  = await auth.getUser();
      var dataRequest = request.all();

      /**
       * VALIDACION DE LOS DATOS DEL FORMULARIO
       */
      const validation = await validate( dataRequest, { 
        license_plate: 'required' ,
        brand_model_id: 'required'
      } );

    
    console.log( dataRequest.license_plate+' '+dataRequest.brand_model_id )
      dataRequest = Object.assign({}, dataRequest, { 
        customers_id: user.id, 
        license_plate: dataRequest.license_plate.toUpperCase(),
        main_picture: ( dataRequest.main_picture != undefined && dataRequest.main_picture.trim() != '' ) ? await this.makeImage(dataRequest.main_picture) : null,
        driver_license_picture: await this.makeImage(dataRequest.driver_license_picture),
        auth_card_picture: await this.makeImage(dataRequest.auth_card_picture),
        insurance_picture: await this.makeImage(dataRequest.insurance_picture)
      });

      if( validation.fails() ) throw validation.message();
       
      // CONSULTA PARA ENCONTRAR EL MODELO PASADO COMO PARAMETRO
      // EXISTE
      const BModel = await BrandModel.find( dataRequest.brand_model_id );

      if( BModel == null ) return response.status(404).send({ error: true, message: 'Invalid params'});

      dataRequest = Object.assign( dataRequest, { brand_id: BModel.id } );


      // SI YA EL USUARIO CONECTADO, POSEE UN VEHICULO EL CUAL
      // license_plate SEA IGUAL AL License_plar QUE LLEGA DE LA SOLICITUD POST
      // Y NO ESTE ELIMINADO (deleted_at == null)
      // ENTONCES SE RETORNA EL YA EXISTENTE
      var vehicle = await Vehicle
      .query()
      .where('license_plate', dataRequest.license_plate)
      .where('customers_id', '=', user.id)
      .whereNull('deleted_at')
      .first();

      if( !vehicle ) vehicle = await Vehicle.create( dataRequest );

      return response.status(202).send( { error: false, vehicle } );

    }catch(error){
      return response.status(500).send({ error, message: "Server error" });
    }
  }

  async show ({ auth, request, response, params }) {

    var user = await auth.getUser();
    var vehicle = await Vehicle
    .query()
    .where('customers_id', '=', user.id)
    .whereNull('deleted_at')
    .where(builder => {
      if(request.user_id != undefined)
        builder.where('id', '=', params.id).orWhere('customers_id', '=', request.user_id);
      else
        builder.where('id', '=', params.id);
    })
    .with('customer')
    .with('transport')
    .first();

    return response.status(202).send({ error: false, vehicle});
  }

  async edit () {
  }

  async update ({ auth, request,response, params }) {
    var user = await auth.getUser();

    var vehicle = await Vehicle
    .query()
    .where('id', '=', params.id)
    .whereNull('deleted_at')
    .where('customers_id', '=', user.id)
    .first();

    if( vehicle ){
      var dataRequest = request.except(['customers_id', 'id']);
      if( dataRequest.license_plate != undefined ) dataRequest.license_plate = dataRequest.license_plate.toUpperCase();

      if( dataRequest.main_picture && dataRequest.main_picture.length > vehicle.main_picture.length && dataRequest.main_picture.trim() != "" ) 
        dataRequest.main_picture = await this.makeImage(dataRequest.main_picture);

      if( dataRequest.driver_license_picture && dataRequest.driver_license_picture.length > vehicle.driver_license_picture.length && dataRequest.driver_license_picture.trim() != "" ) 
        dataRequest.driver_license_picture = await this.makeImage(dataRequest.driver_license_picture);

      if( dataRequest.auth_card_picture && dataRequest.auth_card_picture.length > vehicle.auth_card_picture.length && dataRequest.auth_card_picture.trim() != "" ) 
        dataRequest.auth_card_picture = await this.makeImage(dataRequest.auth_card_picture);

      if( dataRequest.insurance_picture && dataRequest.insurance_picture.length > vehicle.insurance_picture.length && dataRequest.insurance_picture.trim() != "") 
        dataRequest.insurance_picture = await this.makeImage(dataRequest.insurance_picture);

      if( dataRequest.auth_card_picture && dataRequest.auth_card_picture.length > vehicle.auth_card_picture.length && dataRequest.auth_card_picture.trim() != "" ) 
        dataRequest.auth_card_picture = await this.makeImage(dataRequest.auth_card_picture);


      vehicle.merge( dataRequest );
      
      await vehicle.save();
      return response.status(202).send({error: false, vehicle});
    }

    return response.status(404).send({error: false, message: 'Invalid response'});
  }

  async destroy ({ auth, request,response, params }) {
    
    var user = await auth.getUser();
    var vehicle = await Vehicle
    .query()
    .where('customers_id', '=', user.id)
    .where('id', '=', params.id)
    .whereNull('deleted_at')
    .first();

    if( vehicle ) {
      vehicle.deleted_at = moment(new Date()).format('YYYY-MM-DD H:m:s');
      var vehicle = await vehicle.save();
      if( vehicle == true || vehicle == 1 ) return response.status(202).send({ error: false, message: 'Success delete'});
    }

    return response.status(404).send({error: true, message: 'Invalid resource'});

  }

  async types( { response }){
    var types = await Transport.all();

    return response.status(202).send({error: false, types});
  }
}

module.exports = VehicleController
