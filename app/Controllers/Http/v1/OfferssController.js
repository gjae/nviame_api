'use strict'

const Offer = use('App/Models/Offer');
const moment = require('moment');
const DB = use('Database');
const Shipment = use('App/Models/Shipment');
const Delivery = use('App/Models/Delivery');
const { validate } = use('Validator');
const Chat = use('App/Models/Chat');
const Movimiento = use('App/Models/Movimiento');
/**
 * CONTROLADOR PARA EL MANEJO DE LAS OFERTAS PARA LOS VIAJES
 * PARA LAS ACCIONES DE ELIMINAR Y EDITAR, PRIMERO SE CONSULTA SI EL USUARIO CONECTADO
 * ES EL RESPONSABLE DE LA OFERTA ( ASUMIENDO QUE EL ATRIVUTO customers_id HACE REFERENCIA AL DUEÑO DE LA OFERTA )
 * PARA EL METODO RESPONSABLE DE ALMACENAR UNA NUEVA OFERTA PRIMERO SE CONSULTA SI EL USUARIO CONECTADO
 * YA HA REALIZADO UNA OFERTA EN EL SHIPMENT CORRESPONDIENTE, 
 *   SI YA HA HECHO UNA Y NO HA SIDO ELIMINADA ( where deleted_at = NULL) ENTONCES SE RETORNA LA YA EXISTENTE
 * DE CASO CONTRARIO SE CREA UNO NUEVO Y SE RETORNA ESE NUEVO EL EL ENDPOINT
 */
class OfferssController {
  async index ({ auth, response, request }) {
    let user = await auth.getUser();
    var offers = await Offer
    .query()
    .innerJoin('shipments', 'offers.shipments_id', '=', 'shipments.id_shipment' )
    .innerJoin('customers', 'offers.customers_id', '=', 'customers.id')
    .where( build =>{
      return build.where('shipments.state', '=', 2)
        .orWhere('shipments.state', '=', 1);
    } )
    .where('shipments.customers_id', '=', user.id)
    .whereNull('offers.deleted_at')
    .select('offers.*', 'customers.name', 'customers.lastname', 'customers.email', 'customers.image')

    /**
     * EL TOTAL REAL DE ESTRELLAS DE UN USUARIO ES EL TOTAL DE LA SUMA DE SUS ESTRELLAS RECIBIDAS EN LOS ENVIOS
     * DIVIDIDO ENTRE LA CANTIDAD DE ENVIOS CALIFICADOS
     * ES REALMENTE UN PROMEDIO
     */
    for(var i = 0; i < offers.length; i++){
      var stars = await DB.from('ratings').where('to_customers_id', '=', offers[i].customers_id).sum('stars as stars');
      var total_ratings = await DB.from('ratings').where('to_customers_id', '=', offers[i].customers_id).count('id as envios');
      offers[i]['customer_fullname'] = offers[i].name+' '+offers[i].lastname;
      offers[i]['stars'] = stars[0].stars / total_ratings[0].envios;
    }

    return response.status(202).send({error: false, offers, total: offers.length });
  }

  async create () {
  }

  async __hasOfferted( dataRequest, withCount  ){

    if( withCount ){
      let data = await Offer
      .query()
      .where('shipments_id', '=', dataRequest.shipments_id)
      .where('customers_id', '=', dataRequest.customers_id)
      .whereNull('offers.deleted_at')
      .count('* as offers').first();

      return data.offers;
    }

    return await Offer
    .query()
    .where('shipments_id', '=', dataRequest.shipments_id)
    .where('customers_id', '=', dataRequest.customers_id)
    .whereNull('offers.deleted_at').first();
  }

  async store ({ auth, request, response }) {
    try{

      const user = await auth.getUser();
      var dataRequest = request.all();
      dataRequest = Object.assign(dataRequest, { customers_id: user.id });

      const validation = await validate(dataRequest,{
        shipments_id: 'required',
        customers_id: 'required',
        price_offer: 'required',
        type_offer: 'required'
      });

      if( validation.fails() ) { 

        throw 'Invalid params'; 
      }

      let has = await this.__hasOfferted(dataRequest, true);  


      if(  has > 0 ){ 

        offer = await this.__hasOfferted(dataRequest, false);

        return response.status(202).send({ error: false, offer });
      }

      var now = moment( new Date() ).format('YYYY-MM-DD H:mm:ss');

      /**
       * COMO TODOS LOS CAMPOS DE FECHA DE LA TABLA OFFERS SON NOT NULL
       * SE USA LA LIBRERIA moment PARA TOMAR LA HORA DEL SERVIDOR
       * Y AGREGARSELO A TODOS LOS CAMPOS DE FECHA
       */
      
      const shipment = await Shipment.query().where('id_shipment', '=', dataRequest.shipments_id).first();

      if( shipment.customers_id == user.id ) return response.status(202).send({ error: false, message: 'Error: user shipment not valid'});

      var dates = { date_added: now };
      dataRequest = Object.assign(dataRequest, dates, { to_customers_id: shipment.customers_id, accepted_date: null });

      // SI EL TIPO DE PAGO DEL ENVIO (payment_type ) ES 0 ENTONCES EL PRECIO DE LA OFERTA ES EL ESTABLECIDO IGUAL AL QUE FUE
      // PUBLICADO CON EL ENVIO
      // SI NO, ENTOCES SE LE AGREGA EL PRECIO DE LA OFERTA 
      dataRequest.price_offer = ( shipment.payment_type == 0 ) ? shipment.price : dataRequest.price_offer;

      var offer = await Offer.create(dataRequest);
      offer = await Offer.query().where('id', '=' ,offer.id).with('shipment').first();

      return response.status(202).header('Content-Type', 'application/json').send({ error: false , offer })

    }catch(error){
      return response.status(500).header('Content-Type', 'application/json').send({error});
    }
  }

  async show ({ auth, request, response, params }) {

    var offer = await Offer.query()
    .where('id', '=',params.id)
    .with('shipment')
    .with('customer_offer').first();

    return response.status(202).send({ error :false,  offer});

  }

  async edit () {
  }

  async update ({ auth, request, response, params }) {
    
    try{

      var dataRequest = request.all();
      var user = await auth.getUser();
      var offer = await Offer.query().where('id', '=', params.id).whereNull('deleted_at').first();

      
      if( offer.customers_id != user.id ) { throw 'Unauthorized'; }

      offer = await DB.table('offers').where('id', '=', params.id).whereNull('deleted_at').update( dataRequest );
      offer = await Offer.findOrFail(params.id);

      return response.status(202).send({ error: false, offer });
     
    }catch( error ){

      return response.status(404).send({ error: true, message: 'Invalid Resource',  data_error : error });
    }

  }

  async updateState({auth, request, response,params}){

    var dataRequest = request.all();
    var user = await auth.getUser();
    var offer = await Offer.query().where('id', '=', params.id).whereNull('deleted_at').first();

    if( !offer ) return response.status(404).send({ error: true, message: 'Offer not found' });

    var shipment = await Shipment.query().where('id_shipment', '=', offer.shipments_id).first();
      
    if( shipment ){
     
     if( shipment.pin != dataRequest.pin ) return response.status(401).send({error: true, message:'Invalid PIN'});
     if( shipment.state != 6 && dataRequest.status == 8 ) return response.status(202).send({ error: true, message: "Invalid operation: shipment status is not 6 and intent update to status 8" });
      if( shipment.state == 8 ) return response.status(202).send({error: true , message: 'Paquete entregado' });
    //  if( dataRequest.status == 8 && offer.customers_id == user.id ) return response.status(401).send({ error: true, message: 'Unauthorized' });

      shipment.state = dataRequest.status;
      await shipment.save();
      return response.status(202).send({error: false, message: 'Operation success'});

    }


  }

  /**
   * FUNCION PARA ACCEPTAR UNA OFERTA
   * VERIFICA PRIMERO SI LA OFERTA NO ESTA ELIMINADA Y QUE EL USUARIO
   * QUE LA ACEPTA ES EL CREADOR DEL  ENVIO ( TOMANDO EN CUENTA QUE EL USUARIO CONECTADO ES EL USUARIO QUE PUBLICO EL ENVIO )
   * EL ID DEL PARAMETRO CORRESPONDE AL ID DE LA OFERTA QUE SERA ACEPTADA
   * @return {[type]}                  [description]
   */
  async accepted({ request, response, params, auth }){

    var user = await auth.getUser();

    var offer = await DB.from('offers')
    .where('id', '=', params.id)
    .innerJoin('shipments','shipments.id_shipment', '=', 'offers.shipments_id')
    .whereNull('shipments.deleted_at')
    .where('shipments.customers_id', '=', user.id)
    .first();
    if( offer ){  
      var shipment = await Shipment.query().where('id_shipment', '=', offer.shipments_id).first();
      if( offer.accepted_date == null )
      {
        var update = await DB.from('offers').where('id', '=', params.id).update({ accepted_date: moment( new Date() ).format('YYYY-MM-DD HH:mm:ss') });
        if( update ){  
            var offer = await DB.from('offers')
              .where('id', '=', params.id);

            let { customers_id , price_offer} = offer[0];

            // SE CREA UN MOVIMIENTO DE TIPO DEBITO A NOMBRE DEL RESPOSANBLE DEL ENVIO
            // Y UN MOVIMIENTO DE TIPO DIFERIDO A NOMBRE DEL USUARIO DE LA OFERTA ACEPTADA

            shipment.state = 3;
            await shipment.save();
            await Chat.create({
              customers_id: shipment.customers_id,
              to_customers_id: customers_id,
              date_added: moment( new Date ).format('YYYY-MM-DD hh:mm:ss'),
              text: `Usted ha aceptado mi oferta para: ${shipment.package_description}. Ahora podemos chatear.`
            });

            await Chat.create({
              customers_id,
              to_customers_id: shipment.customers_id,
              date_added: moment( new Date ).format('YYYY-MM-DD hh:mm:ss'),
              text: `Su oferta para ${shipment.package_description} ha sido aceptada, ¡que tenga buena suerte!`
            })
            return response.status(202).send({error: false, offer});
        }
      }

      return response.status(404).send({error: true, message: 'Invalid request'});
    }

    return response.status(202).send({error: true, message: 'Invalid Operation'});
  }

  async destroy ({ auth, params, request, response }) {
    var offer = await OfferType
    .query()
    .where('offers.id', '=' ,params.id)
    .whereNull('deleted_at')
    .first();

    var user = await auth.getUser();
    if( offer.customers_id == user.id ){
      offer.deleted_at = moment( new Date() ).format('YYYY-MM-DD H:mm:ss');
      if( offer.save() ) {
        return response.status(202).send({ error : false, message: 'Success deleted' });
      }
    }

    /**
     * SI EL USUARIO CONECTADO NO ES EL DUEÑO DE DETERMINADO REGISTRO EN offers
     * ENTONCES SE RETORNA UN CODIGO 404, IDENTIFICANDO QUE EL RECURSO
     * NO EXISTE O ES INVALIDO
     */
    return response.status(404).send({ error: true, message: 'Invalid resource' });
  }
  
}

module.exports = OfferssController
