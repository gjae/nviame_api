'use strict'

const Country = use('App/Models/Country');
const State = use('App/Models/State');

class CountryController {
  async index ({ request, response }) {

    var countries = await Country.all();

    return response.status(202).send({ error: false, countries });
  }

  async create () {
  }

  async store () {
  }

  async show ({ request, response, params }) {

    var states = await State.query().where('country_id', '=', params.id);

    return response.status(202).send({ error: false , states });

  }

  async edit () {
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = CountryController
