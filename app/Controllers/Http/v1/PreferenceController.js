'use strict'

const DB  = use('Database');
const moment = require('moment');
const Transport = use('App/Models/Transport');
const Preference = use('App/Models/Preference');
const PrefTransp = use('App/Models/PrefTransp');

class PreferenceController {
  async index ({ auth, request, response }) {
    const user = await auth.getUser();

    const preferences = await Preference
    .query()
    .where('customers_id', '=', user.id)
    .with('transports')
    .first();

   

    var rates = await DB
    .from('rates')
    .where('customers_id', '=', user.id)
    .first();


    var availables_transports = await DB
    .from('transports');

    return response.status(202).send({ error: false, preferences , rates, availables_transports});
  }

  async create () {
  }

  async store () {
  }

  async show () {
  }

  async edit () {
  }

  async update ({ auth, request, response }) {

    const user = await auth.getUser();
    var dataRequest = request.all();

    switch( dataRequest.type ){
      case 'transport': {

        var prefs = await Preference.query().where('customers_id', '=', user.id).first();

        var pref = await PrefTransp.query()
        .where('preferences_id', '=', prefs.id)
        .where('transports_id', '=', dataRequest.transport_id)
        .first();


        pref.actived_at = ( dataRequest.status == 1 ) ? moment(new Date()).format('YYYY-MM-DD HH:mm:ss') : null;
        pref.status = dataRequest.status;

        await pref.save();
        break;
      }

      case 'preference':{
        // dataRequest;
        var prefs = await Preference.query().where('customers_id', '=', user.id).first();


        prefs[ dataRequest.update ] = dataRequest.value;
        prefs[ dataRequest.update == 'fixed_price' ? 'rate_per_km' : 'fixed_price' ] = 0;

        await prefs.save();
        break;
      }
    }

    const preferences = await Preference
    .query()
    .where('customers_id', '=', user.id)
    .with('transports')
    .first();

   

    var rates = await DB
    .from('rates')
    .where('customers_id', '=', user.id)
    .first();


    var availables_transports = await DB
    .from('transports');

    return response.status(202).send({ error: false, preferences , rates, availables_transports});

  }

  async destroy () {
  }
}

module.exports = PreferenceController
