'use strict'
const Customer = use('App/Models/Customer');
const Shipment = use('App/Models/Shipment');
const DB = use('Database');
const crypto = require('crypto');
const Rating = use('App/Models/Rating');
const Job = use('App/Models/Job');
const { validate } = use('Validator');
const moment = require('moment');
const Offer = use('App/Models/Offer');
const Trip = use('App/Models/Trip');


const Helpers = use('Helpers')

const buffer = require('buffer');
const fs = require('fs');


var base64Img = require('base64-img');

/**
 * CONTROLADOR PARA RECIBIR LAS PETICIONES CON EL PREFIJO V1/CUSTOMER
 * POSIBLES CODIGOS DE ERROR: 
 * |__ 401: UNAUTHORIZED (EN CASO DE QUE EL LOGIN O LA AUTORIZACION DEL TOKEN NO SEA CORRECTA)
 * |__ 202, 201, 200: OK, LA PETICION HA SIDO ACEPTADA
 * |__ 500: ERROR INTERNO DEL SERVIDOR
 *
 * @author Giovanny Avila gjavila1995@gmail.com
 * @date 13 Apr, 2018
 */
class CustomerController {

  async index ({ request }) {
    return { hola: 'hola mundo' };
  }

  async create () {
  }

  async store ({request, response, params, view}) {
    var dataRequest = request.all();
    const validation = await validate( dataRequest, {
      email: 'required',
      password: 'required',
    });

    if( validation.fails() ) return response.status(202).send({ error: true, message: 'Already in use' });

    var customer = await Customer.query().where('email', '=', dataRequest.email).first();

    if( customer ) return response.status(202).send({ error: true, message: 'Email error' });

    var password = crypto.createHash('md5').update(dataRequest.password).digest('hex');
    dataRequest = Object.assign(dataRequest, {
      password
    }); 

    const nodemailer = require('nodemailer');


    customer = await Customer.create(dataRequest);
    customer = await Customer.find(customer.id);

    let pinLen = ""+customer.id;

    for(var i= 0; i< 6 - pinLen.length; i++){
      pinLen += '0';
    }

    await Customer.query().where('id', '=', customer.id).update({verify_pin: pinLen});
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {
          // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
              user: 'gjavila1995@gmail.com', // generated ethereal user
              pass: '23458706627267' // generated ethereal password
            }
        });

          // setup email data with unicode symbols
          let mailOptions = {
              from: '"Confirmacion de registro" <registro@nviame.com>', // sender address
              to: `${dataRequest.email}`, // list of receivers
              subject: 'Verificacion de usuario NVIAME', // Subject line
              html: view.render('mail', {pin: pinLen})// html body
          };

          // send mail with defined transport object
          transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  return console.log(error);
              }
              console.log('Message sent: %s', info.messageId);
              // Preview only available when sending through an Ethereal account
              console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

              // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
              // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
          });
    });

    if( customer ) return response.status(202).send({ error: false, customer });

  }

  async show ({auth, response, request, params}) {

    try{
      var references = null

      var pending = await DB.table('shipments').where('customers_id', '=', params.id)
      .where( query => {
        return query.orWhere('state', '=','6').orWhere('state', '=', 2).orWhere('state', '=', 3).orWhere('state','=', 7 );
      }).count('* as pending').first();

      var concreted = await DB.table('shipments')
      .where('state', '=', 8).where('customers_id', '=', params.id).count('* as concreted').first();
      
      var offers = await Offer
      .query()
      .innerJoin('shipments', 'offers.shipments_id', '=', 'shipments.id_shipment')
      .where('offers.to_customers_id', '=', params.id)
      .where( 'shipments.state', '=', 2)
      .count('offers.id as offers')
      .first();

      var shipments_made = await Shipment
      .query()
      .innerJoin('offers', 'offers.shipments_id', '=', 'shipments.id_shipment')
      .where('shipments.customers_id', '=', params.id)
      .where((builder) =>{
        return builder.where('state', '=', 8);
      })
      .count('* as mades')
      .first();


      var deliveries = await DB.from('offers')
      .where('customers_id', '=', params.id)
      .whereNull('deleted_at').whereNotNull('concreted_at').count('id as deliveries');



      var rating = await Rating.query().where('to_customers_id', '=', params.id).sum('stars as stars').first();
      var ratings = await Rating.query().where('to_customers_id', '=', params.id).count('id as ids').first();

      if( request.all().with != undefined && request.all().with == 'references' )
        references = await Rating.query().where('to_customers_id', '=', params.id).with('from').fetch();
      
      var customer = await Customer
      .query()
      .where('customers.id', '=', params.id)
      .leftJoin('jobs', 'customers.job', '=', 'jobs.id')
      .with('city')
      .with('state')
      .with('country')
      .first()

      customer.id = parseInt(params.id);

      var trips = await Trip.query()
      .where('customers_id', '=', customer.id)
      .where('status', '=', 1)
      .where('to_date', '>', moment(new Date).format('YYYY-MM-DD'))
      .count("* as trips");

      var devolutions = await Shipment.query()
      .where('customers_id', '=', customer.id)
      .where('state', '=', 13)
      .whereNull('deleted_at')
      .count('* as devolutions');

      var resp = {
        customer,
        ...pending,
        ...concreted,
        ...offers,
        stars: rating.stars / ratings.ids,
        deliveries: deliveries[0].deliveries,
        ...shipments_made,
        trips: trips[0].trips,
        devolutions: devolutions[0].devolutions
      }

      if( references !== null )
        resp.references = references;
      
      return response
        .header('Content-Type', 'application/json')
        .status(202)
        .send( { error: false, ...resp } );

    }catch(error){
      return response.header('Content-Type', 'application/json').status(502).send({ error: true,  error });
    }

  }

  async edit () {
  }

  async verify_code({ request, response, params }){

    let customer = await Customer.query().where('verify_pin', '=', params.pin)
    .setVisible(['verify_pin', 'email']).first();
    if( customer )
      return response.status(202).send({error: false, customer});
    
    return response.status(202).send({error: true, message: "Invalid code"});
  }

  async reset({ request, response }){
    const dataRequest = request.all();
    let validation = await validate(dataRequest,{
      email: 'required',
      pin: 'required',
      password: 'required'
    });

    if( validation.fails() ) return response.status(202).send({error: true, message: 'Invalid argument'});

    let customer = await Customer.query().where(build =>{
      return build.where('verify_pin', '=', dataRequest.pin).where('email', '=', dataRequest.email)
    }).update({ password:  crypto.createHash('md5').update(dataRequest.password).digest('hex') });
    if( !customer )
      return response.status(202).send({error: true, message: "Invalid email or pin"});
    if( customer )
      return response.status(202).send({error: false, message: "Password reset"});
  }

  async update ({ auth, request, response, params }) {
    // VERIFICACION DE QUE EXISTA PREVIAMENTE UNA SESSION ACTIVA
    // DE NO HABER NINGUNA SESSION ACTIVA, SE RETORNA UN CODIGO 401
    // DE HABER UNA SESSION ACTIVA, SE EXTRAEN TODOS LOS DATOS DEL OBJETO REQUEST
    // SE VERIFICA QUE EL PARAMETRO PASSWORD ESTE PRESENTE EN EL REQUEST Y SE HACE 
    // QUE SE CREE UN HASH MD5 CON LA LIBRERIA CRYPTO DE NODEJS
    // LUEGO SE BUSCA EL REGISTRO CUSTOMER CORRESPONDIENTE AL ID DE ENTRADA
    // Y SE RETORNA LA RESPUESTA
   
      try{
        var data = request.all();
        for(var propName in data){
          if(data[propName] === null || data[propName] === undefined || data[propName] === "" ){
            delete data[propName];
          }
        }
        if(data.password !== undefined) 
          data.password = crypto.createHash('md5').update(data.password).digest('hex');
        if( data.city != undefined ){
          let city = await DB.from('cities').where('city_name', 'LIKE', `%${data.city}%`).select('id').first();
        	data.city_id = city.id;
        	delete data.city;
        }

        // VERIFICA QUE DE LA PETICION LLEGUE EL ATRIBUTO IMAGE Y NO ESTE VACIO,
        // EN EL CASO DE EXISTIR Y ESTAR VACIO, LO QUITA DEL OBJETO
        if( data.image != undefined && data.image != "" ){

          let filename = crypto.createHash('md5').update( moment( new Date() ).format('YYYY-MM-DD HH:MM:SS') ).digest('hex');
          console.log(filename);
          let path = base64Img.imgSync(data.image.trim() , Helpers.publicPath('img/uploads') , filename);

          filename = path.split('/');

          data.image = ( path ) ? filename[ (filename.length - 1) ] : null;

        }else if(data.image != undefined && data.image ==""){ 
          delete data.image;  
        }
	
        var cust = await Customer.query().where('id', '=',params.id).first();

        for( var object in data ){
          cust[object] = data[object];
        }
        cust.save();

        var customer = await Customer
        .query()
        .where('id', '=', params.id)
        .with('city')
        .with('state')
        .with('country')
        .first();
        return response
          .status(202)
          .header('Content-Type','application/json')
          .send({ error:false, customer });
        

      }catch(error){
        return 
          response
            .header('Content-Type', 'application/json')
            .status(503)
            .send({ error: true, message: 'Service Unavailable on customer update', error }); 

      }

  }

  async destroy () {
  }

  /**
   * METODO PARA REALIZAR EL LOGIN, RETORNA UN JSON 
   * CON LOS DATOS DEL USUARIO YA AUTENTICADO
   */
  async login ({ request, auth, response }){
    var data = request.only(['email', 'password']);
    data.password = crypto.createHash('md5').update(data.password).digest('hex');
    var customer = await Customer
    .query()
    .where( 'email', '=', data.email )
    .where( 'password', '=',  data.password)
    .with('city')
    .with('state')
    .with('country')
    .with('jobs')
    .first();

    var customer2 = await Customer
    .query()
    .where( 'email', '=', data.email )
    .where( 'password', '=',  data.password)
    .first();

    var responseAuth = {
      customer: {},
      auth: {},
      status: 401,
    };

    customer = ( customer == null && customer2 != null ) ? customer2 : customer;
   if( customer && customer.verify_at != null){
    responseAuth.customer = customer;
    responseAuth.auth =  await auth.generate(responseAuth.customer);
    responseAuth.status = 202;
   }
   else{
    response
    .header('Content-Type', 'application/json')
    .status(401)
    .send ({
      error: true,
      message: 'Email or password invalid'
    });
   }

    return response
    .header('Content-Type', 'application/json')
    .status(responseAuth.status)
    .send(responseAuth);
  }

  async jobs({ response }){
    var job = await Job.all();
    return response.status(202).send({ error: false , jobs: job});
  }


  async verify({params, request, response}){
    var customer = await Customer
    .query().where('verify_pin', '=', request.all().pin)
    .whereNull('verify_at')
    .first();
    if( customer ){
      await Customer
      .query().where('verify_pin', '=', request.all().pin)
      .update({ 'verify_at': moment(new Date()).format('YYYY-MM-DD  HH:mm:ss') });

      return response
      .header('Content-Type', 'application/json')
      .status(202)
      .send({error: false, message: 'Accepted'});
    }

    return response
    .header('Content-Type', 'application/json')
    .status(404)
    .send({error: true, message: 'Resource error'});
  }

  async restore_mail({params, request, response, view}){
    var dataRequest =  request.all();

    if( request.all().email !== undefined && request.all().email != null ){
      let customer = await Customer.query().where('email', '=', request.all().email ).first();
      
      if(customer){

          let { identification } = customer;

          identification = ( identification == null ) ? '12345678910' : identification;
          let pin = '';
          for( var i = 0; i <4 ; i++){
            pin += identification.charAt( 
              Math.floor( Math.random() * identification.length ) 
            );
          }
          await Customer.query().where('email', '=', customer.email).update({verify_pin: pin});

          const nodemailer = require('nodemailer');
         
          nodemailer.createTestAccount((err, account) => {
                // create reusable transporter object using the default SMTP transport
              let transporter = nodemailer.createTransport({
                  service: 'Gmail',
                  auth: {
                    user: 'gjavila1995@gmail.com', // generated ethereal user
                    pass: '23458706627267' // generated ethereal password
                  }
              });
                // setup email data with unicode symbols
                let mailOptions = {
                    from: '"Restauracion de usuario" <registro@nviame.com>', // sender address
                    to: `${dataRequest.email}`, // list of receivers
                    subject: 'PIN de verificacion de usuario', // Subject line
                    html: view.render('restore_pin', {pin: pin})// html body
                };

                // send mail with defined transport object
                
                transporter.sendMail(mailOptions,(error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    

                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
                });
          });

        return response.status(202).send({error: false, message:'Send Email'});
      }
    }

    return response.status(202).send({error: true, message: 'Invalid email'});

  }

}

module.exports = CustomerController
