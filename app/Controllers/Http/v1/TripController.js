'use strict'

const Trip = use('App/Models/Trip');
const City = use('App/Models/City');
const Scale = use('App/Models/Scale');
const DB = use('Database');
const moment = require('moment');

class TripController {
   
  async index ({auth, request, response }) {
    const user = await auth.getUser();

    var trips = await Trip.query().where('customers_id', '=', user.id).whereNull('deleted_at').with('scales', builder=>{
      return builder.whereNull('deleted_at');
    }).fetch();
    return response.status(202).send({ error: false, trips});
  }

  async create () {
  }

  async store ({ auth, request, response }) {
    const user = await auth.getUser();
    const city_to = await City.query().where('city_name', 'LIKE', '%'+request.all().city_to+'%').first();
    const city_from = await City.query().where('city_name', 'LIKE', '%'+request.all().city_from+'%').first();
    var req = request.except(['scales']);
    var {scales} =  request.only(['scales']);

    req.city_to = city_to.id;
    req.city_from = city_from.id;
    req.customers_id = user.id;
    req.to_date = ( req.to_date == "" ) ? null : req.to_date;

    var trip = await Trip.create( req );
    if( Array.isArray(scales) && scales.length > 0 ){
       for (var i = 0; i < scales.length ; i++){
        var city = await City.query().where('city_name', 'LIKE', '%'+scales[i].city+'%').first();

        var scale = await DB.from('scales').insert({ 
            trip_id: trip.id, 
            city_id: city.id, 
            address: scales[i].address, 
            lat: scales[i].lat, 
            long: scales[i].long, 
            placeid: scales[i].placeid  
        });

      }
    }

    trip = await Trip.query().where('id_trip', '=', trip.id).whereNull('deleted_at').with('scales').first();
    return response.status(202).send({ error: false, trip});

  }

  async show ({ auth, request, response, params }) {

    const trip = await Trip.query().where('id_trip', '=', params.id).whereNull('deleted_at').with('scales', builder=>{
      return builder.whereNull('deleted_at');
    }).first();

    if( trip )
      return response.status(202).send({error: false, trip});

    return response.status(404).send({error: true, message: "Invalid Resource"});
  }

  async edit () {
  }

  async update ({ request, response, params, auth }) {
    const user = await auth.getUser();
    var trip = await Trip.query().where('id_trip', '=', params.id).where('customers_id', '=', user.id ).whereNull('deleted_at').first();
    if( !trip ){
      return response.status(404).send({error: true, message: 'Invalid Resource'});
    }
    var dataUpdate = request.except(['id', 'customers_id', 'scales']);

    if(dataUpdate.city_from !== undefined){
      if( typeof( dataUpdate.city_from ) == 'string' ){
        var city_from = await City.query().where('city_name', 'LIKE', '%'+dataUpdate.city_from+'%').first();
        dataUpdate.city_from = city_from.id;
      }
    }
    if( dataUpdate.city_to !== undefined ){
      if( typeof( dataUpdate.city_to ) ){
        var city_to = await City.query().where('city_name', 'LIKE', '%'+dataUpdate.city_to+'%').first();
        dataUpdate.city_to = city_to.id;
      }
    }

    trip = await DB.from('trips').where('id_trip', '=', params.id).update( dataUpdate );
    trip = await Trip.query().where('id_trip', '=', params.id).whereNull('deleted_at').with('scales').first();

    return response.status(202).send({error: false, trip});

  }

  async updatScales({ auth, request, response, params }){

    var scaleId = params.id;
    const user = await auth.getUser();

    var trip = await Trip.query()
    .whereNull('trips.deleted_at')
    .whereNull('scales.deleted_at')
    .where('customers_id', '=', user.id)
    .innerJoin('scales', 'scales.trip_id', '=', 'trips.id_trip')
    .where('scales.id', '=', scaleId )
    .first();

    if(! trip)
      return response.status(404).send({ error: false, message: 'Invalid Resource'});

    var { scales } = request.only(['scales']);
    if( Array.isArray( scales ) && scales.length > 0 ){

      for(var i = 0; i < scales.length; i++){
        if( typeof( scales[i].city ) == 'string' ){
          var city = await City.query().where('city_name', 'LIKE', '%'+scales[i].city+'%').first();
          scales[i].city = city.id;
        }

        var update = await DB.from('scales').where('id', '=', scaleId).update({
          city_id: scales[i].city,
          address: scales[i].address,
          lat: scales[i].lat,
          long: scales[i].long,
          placeid: scales[i].placeid
        });
      }

    }

    var scale = await Scale.query().whereNull('deleted_at').where('id', '=', params.id).first();
    return response.status(202).send({error: false, scale});
  }

  async deletedScales({ params, response, auth }){
    var scaleId = params.id;
    const user = await auth.getUser();

    var trip = await Trip.query()
    .whereNull('trips.deleted_at')
    .whereNull('scales.deleted_at')
    .where('customers_id', '=', user.getUser())
    .innerJoin('scales', 'scales.trip_id', '=', 'trips.id_trip')
    .where('scales.id', '=', scaleId )
    .first();

    if(! trip)
      return response.status(404).send({ error: false, message: 'Invalid Resource'});

    var deleted = await DB.from('scales').where('id', '=', scaleId).update({
      deleted_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    });

    if( deleted )
      return response.status(202).send({error: false, message: 'Success operation'});

    return response.status(500).send({error: true, message: 'Internal error server'});
  }

  async destroy ({ params, response, auth }) {

    const user = await auth.getUser();

    var trip = await Trip.query().where('id_trip', '=', params.id).where('customers_id', '=', user.id).whereNull('deleted_at').first();
    if(trip){ 
      var del = await DB.from('trips').where('id_trip', '=', params.id).update({ deleted_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') });
      if(del)
        return response.status(202).send({ error: false, message: "Success operation" });

      return response.status(202).send({ error: true, message: "Internal server error" });
    }
    
    return response.status(202).send({ error: true, message: "Resource not fount" });
  }
}

module.exports = TripController
