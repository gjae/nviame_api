'use strict'

const Customer = use('App/Models/Customer');
const SocialSig = use('App/Models/Socialsig');


class FacebookController {
  async index () {
  }

  async create () {
  }

  async store ({ auth, request, params, response }) {

    var dataRequest = request.all();
    var user = await SocialSig
    .query()
    .where( query =>{
      return query.where('profile_id','=', dataRequest.profile_id)
      .where('network', '=', dataRequest.network);
    }).with('customer').first();


    if( !user ){

      var customer = await Customer.create({
        name: dataRequest.name,
        lastname: dataRequest.lastname,
        email: dataRequest.email
      });
      var social = await SocialSig.create({ 
        profile_id: dataRequest.profile_id,
        network: dataRequest.network,
        customer_id: customer.id,
        accessToken: dataRequest.accessToken
      });

	var login = await auth.generate(customer);
      return response.status(202).send({ error: false,user: { customer, social, auth: login }  });
    }

    return response.status(202).send({ error: false,  user });

  }

  async show ({ auth, request, response }) {
    var dataRequest = request.all();

    var user = await SocialSig.query()
    .where( query =>{
      return query.where('profile_id', '=', dataRequest.profile_id)
      .where('network', '=', dataRequest.network);
    }).with('customer').first();

    if( user ) {
      var customer = await Customer.find( user.customer_id );

      var auth_token = await auth.generate(customer);
      user.auth = auth_token;
      return response.status(202).send({ error: false, user });
    };
    return response.status(202).send({ error: true, message: 'User not found' });

  }

  async edit () {
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = FacebookController
