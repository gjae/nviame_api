'use strict'

const Brand = use('App/Models/Brand');
const BrandModels = use('App/Models/Bmdl');

class VehicleBrandController {
  async index ({ response }) {
    const brands = await Brand
    .query()
    .whereNull('deleted_at')
    .select('id', 'brand_name');

    return response.status(202).send({error: false, brands});

  }

  async create () {
  }

  async store () {
  }

  async show ({ response, params }) {

    var models = await BrandModels
    .query()
    .where('brand_id', '=', params.id)
    .whereNull('deleted_at')
    .select('id', 'model_name');

    return response.status(202).send({ error: false, models});

  }

  async edit () {
  }

  async update () {
  }

  async destroy () {
  }
}

module.exports = VehicleBrandController
